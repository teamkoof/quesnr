#############################################################
# Version:0_0_1
# Name: Ruchira
# Date: 13 Feb 2018
#############################################################
CREATE TABLE address (address_id BIGINT NOT NULL AUTO_INCREMENT, address_line1 VARCHAR(255), address_line2 VARCHAR(255), city VARCHAR(255), state VARCHAR(255), country VARCHAR(255), zip_code VARCHAR(255), PRIMARY KEY (address_id));

CREATE TABLE country (country_id BIGINT NOT NULL AUTO_INCREMENT, country_name VARCHAR(255) NOT NULL, country_code VARCHAR(255) NOT NULL, PRIMARY KEY (country_id));

CREATE TABLE USER (user_id BIGINT NOT NULL AUTO_INCREMENT, user_name VARCHAR(255) NOT NULL UNIQUE, PASSWORD VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, ip_address VARCHAR(255) NOT NULL, j_session_id VARCHAR(255) NOT NULL, created_by VARCHAR(255) NOT NULL, date_created DATETIME NOT NULL, updated_by VARCHAR(255) NOT NULL, updated_date DATETIME NOT NULL, enabled BOOLEAN, verified BOOLEAN, login_count BIGINT, last_loggedInTime DATETIME NOT NULL, role VARCHAR(255) NOT NULL, contact_no VARCHAR(255), user_address BIGINT NOT NULL, PRIMARY KEY (user_id));

CREATE TABLE user_role (user_role_id BIGINT NOT NULL AUTO_INCREMENT, user_role VARCHAR(255) NOT NULL, role VARCHAR(255) NOT NULL, user_id BIGINT NOT NULL, PRIMARY KEY (user_role_id));

ALTER TABLE USER ADD INDEX (user_address), ADD CONSTRAINT FOREIGN KEY (user_address) REFERENCES address (address_id);

ALTER TABLE user_role ADD INDEX  (user_id), ADD CONSTRAINT  FOREIGN KEY (user_id) REFERENCES USER (user_id);

CREATE TABLE properties (property_id BIGINT NOT NULL AUTO_INCREMENT,property_name VARCHAR(255) NOT NULL,property_value VARCHAR(255) NOT NULL, PRIMARY KEY (property_id));
 
INSERT INTO `properties` (`property_id`,`property_name`,`property_value`) VALUES (1, 'version','1.0');

INSERT INTO `country` (`country_id`,`country_name`, `country_code`) VALUES (1, 'INDIA','IN');
INSERT INTO `country` (`country_id`,`country_name`, `country_code`) VALUES (2, 'AMERICA','USA');

############################################################################################################################
#Name: Raviraj M
#Date: 19-Feb-2018
#Truncating the tables 
#use this when need to delete records from the table having foreign key constraints
############################################################################################################################
SELECT * FROM USER;
SELECT * FROM address;
SELECT * FROM user_role;

SET FOREIGN_KEY_CHECKS = 0; 
TRUNCATE TABLE  USER; 
TRUNCATE TABLE  address; 
TRUNCATE TABLE  user_role; 
SET FOREIGN_KEY_CHECKS = 1;

############################################################################################################################
#Name: Raviraj M
#Date: 22-Feb-2018
#add column in user table -
# basic_details
# additional columns in student 
############################################################################################################################

ALTER TABLE USER ADD COLUMN basic_details BOOLEAN DEFAULT FALSE AFTER institute_id;

ALTER TABLE student 
ADD COLUMN board_id BIGINT(20) NOT NULL AFTER address_id,
ADD COLUMN board_name VARCHAR(100) AFTER board_id, 
ADD COLUMN class BIGINT(20) NOT NULL AFTER board_name, 
ADD COLUMN school VARCHAR(250) AFTER class, 
ADD COLUMN school_city VARCHAR(100) AFTER school, 
ADD COLUMN school_state VARCHAR(100) AFTER school_city;



