/*
SQLyog Community v8.81 
MySQL - 5.1.73-community : Database - quesnr
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`quesnr` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `quesnr`;

/*Data for the table `address` */

insert  into `address`(`address_id`,`address_line1`,`address_line2`,`city`,`state`,`country`,`zip_code`) values (1,NULL,NULL,'pune','Maharashtra','India','411017'),(2,NULL,NULL,'pune','Maharashtra','India','411017'),(3,NULL,NULL,'pune','Maharashtra','India','411017');

/*Data for the table `board` */

insert  into `board`(`board_id`,`board_name`,`board_code`,`board_state`) values (1,'Andhra Pradesh Board of Secondary Education','APSEB','Andhra Pradesh'),(2,'Andhra Pradesh Board of Intermediate Education','APIEB','Andhra Pradesh'),(3,'Andhra Pradesh Open School Society','APOS','Andhra Pradesh'),(4,'Assam Board of Secondary Education','ASSE','Assam'),(5,'Grameen Mukt Vidyalaya Siksha Sansthan','GMVSS','Delhi'),(6,'Assam Higher Secondary Education Council','AHSE','Assam'),(7,'Assam State Open School	','ASOS','Assam'),(8,'Bihar Board of Open Schooling & Examination','BOSEB','Bihar'),(9,'Bihar School Examination Board','BSEB','Bihar'),(10,'Board of Higher Secondary Education Delhi','BHSE','Delhi'),(11,'Maharashtra State Board of Secondary and Higher Secondary Education','MSBSHE','Maharashtra'),(12,'Chhattisgarh Secondary Board of Education','CHSBE','Chhattisgarh'),(13,'Council for the Indian School Certificate Examinations ','CISCE','Delhi'),(14,'Goa Board of Secondary & Higher Secondary Education	','GBSHE','Goa'),(15,'Gujarat Secondary Education Board','GJSEB','Gujarat'),(16,'Gujarat State Open School','GSOS','Gujarat'),(17,'Haryana Board of School Education','HBSE','Haryana'),(18,'Haryana State Open School','HSOS','Haryana'),(19,'Himachal Pradesh Board of School Education','HPBSE','Himachal Pradesh'),(20,'Himachal Pradesh State Open School','HPSOS','Himachal Pradesh'),(21,'Indian Board of School Education','IBSE','West Bengal'),(22,'Jammu and Kashmir State Board of School Education','JKSB','Jammu and Kashmir'),(23,'Jammu and Kashmir State Open School','JKSOS','Jammu and Kashmir'),(24,'Jharkhand Academic Council','JAC','Jharkhand'),(25,'Karnataka Secondary Education Examination Board	','KSEEB','Karnataka'),(26,'Kerala Higher Secondary Examination Board','KHSEB','Kerala'),(27,'Maharashtra State Board of Secondary and Higher Secondary Education','MSBSHE','Maharashtra'),(28,'Madhya Pradesh Board of Secondary Education','MPSEB','Madhya Pradesh'),(29,'Madhya Pradesh State Open School','MPSOS','Madhya Pradesh'),(30,'Maharashtra State Board of Secondary and Higher Secondary Education','MSBSHE','Maharashtra'),(31,'Meghalaya Board of School Education','MEGSEB','Meghalaya'),(32,'Mizoram Board of School Education','MZSEB','Mizoram'),(33,'Nagaland Board of School Education','NGSEB','Nagaland'),(34,'National Institute of Open Schooling ','NIOS','Uttar Pradesh'),(35,'Odisha Board of Secondary Education','OSEB','Odisha'),(36,'Odisha Council of Higher Secondary Education','OCHSE','Odisha'),(37,'Punjab School Education Board','PSEB','Punjab'),(38,'Rajasthan Board of Secondary Education','RJBSE','Rajasthan'),(39,'Rajasthan State Open School','RSOS','Rajasthan'),(40,'Tamil Nadu Board of Secondary Education	','TNSEB','Tamil Nadu'),(41,'Telangana Board of Intermediate Education','TGIEB','Telangana'),(42,'Telangana Board of Secondary Education','TGSEB','Telangana'),(43,'Tripura Board of Secondary Education','TRPSEB','Tripura'),(44,'Uttar Pradesh Board of High School and Intermediate Education','UPHSIEB','Uttar Pradesh'),(45,'Uttarakhand Board of School Education','UTRSEB','Uttarakhand'),(46,'West Bengal Board of Madrasah Education','WBMEB','West Bengal'),(47,'West Bengal Board of Primary Education','WBPEB','West Bengal'),(48,'West Bengal Board of Secondary Education','WBSEB','West Bengal'),(49,'West Bengal Council of Higher Secondary Education','WBCHSE','West Bengal'),(50,'West Bengal Council of Rabindra Open Schooling','WBCROS','West Bengal');

/*Data for the table `country` */

/*Data for the table `institute` */

/*Data for the table `student` */

insert  into `student`(`student_id`,`first_name`,`last_name`,`email`,`contact_no`,`class`,`school`,`school_city`,`school_state`,`address_id`,`board_id`) values (1,'Raviraj','Mishra','ravirajmishra98@gmail.com','7387531101',10,'Jai Hind High School and Jr. College','Pune','Maharashtra',1,30),(2,'Mahendra','Bhamu','ravirajmishra98@gmail.com','7387531101',10,'Kendriya Vidyalay, Southern Command','Pune','Maharashtra',2,11),(3,'Ruchira','Tilekar','ravirajmishra98@gmail.com','7387531101',0,NULL,NULL,NULL,3,NULL);

/*Data for the table `user` */

insert  into `user`(`user_id`,`user_name`,`password`,`ip_address`,`j_session_id`,`created_by`,`date_created`,`updated_by`,`updated_date`,`enabled`,`verified`,`login_count`,`last_loggedInTime`,`role`,`agree_terms`,`basic_details`,`profile_pic`,`student_id`,`institute_id`) values (1,'quesnrstudent','admin','192.168.111.91','105FF08781D1F70A1AFB617AFA55A7CF','ROLE_STUDENT','2018-03-06 13:40:06','ROLE_STUDENT','2018-03-06 13:40:06',1,1,46,'2018-03-08 00:56:14','ROLE_STUDENT',1,1,NULL,1,NULL),(2,'mahitest','admin','192.168.0.100','DEC339AF18D935F7D589795CEA6E0A21','ROLE_STUDENT','2018-03-07 09:35:12','ROLE_STUDENT','2018-03-07 09:35:12',1,1,2,'2018-03-07 09:36:17','ROLE_STUDENT',1,1,NULL,2,NULL),(3,'ruchiratest','admin','192.168.111.91','399D4158253F703E7CE9EB8FC1289701','ROLE_STUDENT','2018-03-07 11:39:19','ROLE_STUDENT','2018-03-07 11:39:19',1,1,0,'2018-03-07 11:39:19','Student',1,0,NULL,3,NULL);

/*Data for the table `user_role` */

insert  into `user_role`(`user_role_id`,`user_role`,`role`,`user_id`) values (1,'ROLE_STUDENT','ROLE_STUDENT',1),(2,'ROLE_STUDENT','ROLE_STUDENT',2),(3,'ROLE_STUDENT','ROLE_STUDENT',3);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
