<!DOCTYPE HTML>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
<title>Quesnr | Login </title>
</head>
<body>
	<h2>Signup here</h2>
	<form:form action="doSignup" method="POST" modelAttribute="user" name="signupform" id="signupform">
		<table border="1">
			<tr>
				<td><form:label path="username"><spring:message code="user.username"/></form:label></td>
				<td><form:input path="username" placeholder="Enter Username" /></td>
			</tr>
			<tr>
				<td><form:label path="password"><spring:message code="user.password"/></form:label></td>
				<td><form:password path="password" placeholder="Enter Password"/></td>
			</tr>
			<tr>
				<td><form:label path="firstName"><spring:message code="user.firstName"/></form:label></td>
				<td> <form:input path="firstName" placeholder="Enter First Name"/></td>
			</tr>
			<tr>
				<td><form:label path="lastName"><spring:message code="user.lastName"/></form:label></td>
				<td> <form:input path="lastName" placeholder="Enter Last Name"/></td>
			</tr>
			<tr>
				<td><form:label path="email"><spring:message code="user.email"/></form:label></td>
				<td> <form:input path="email" placeholder="Enter Email"/></td>
			</tr>
			<tr>
				<td><form:label path="userAddress.addressLine1"><spring:message code="address.addressLine1"/></form:label></td>
				<td> <form:input path="userAddress.addressLine1" placeholder="EnterAddress Line 1"/></td>
			</tr>
			<tr>
				<td><form:label path="userAddress.addressLine2"><spring:message code="address.addressLine2"/></form:label></td>
				<td> <form:input path="userAddress.addressLine2" placeholder="Enter Address Line 2"/></td>
			</tr>
			<tr>
				<td><form:label path="userAddress.city"><spring:message code="address.city"/></form:label></td>
				<td> <form:input path="userAddress.city" placeholder="Enter City"/></td>
			</tr>
			<tr>
				<td><form:label path="userAddress.state"><spring:message code="address.state"/></form:label></td>
				<td> <form:input path="userAddress.state" placeholder="Enter State"/></td>
			</tr>
			<tr>
				<td><form:label path="userAddress.country"><spring:message code="address.country"/></form:label></td>
				<td> <form:select path="userAddress.country" items="${countryList}"/></td>
				
			</tr>
			<tr>
				<td><form:label path="userAddress.zipCode"><spring:message code="address.zipCode"/></form:label></td>
				<td> <form:input path="userAddress.zipCode" placeholder="Enter Zip code"/></td>
			</tr>
			
			<tr>
				<td></td>
				<td><input type="submit" value="SIGNUP"></td>
			</tr>
		</table>
	</form:form>
</body>
</html>