<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="org.springframework.security.core.GrantedAuthority" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Quesnr | Home</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.js"></script>  
<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>  
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"/>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"> </script>

<script src="views/resources/js/quesnr-js/generic_js.js"></script>
<script src="views/resources/js/quesnr-js/validate_js.js"></script>


<!-- manual styles -->
<link rel="stylesheet" href="views/resources/css/tilesStyle.css" type="text/css">
<!-- fonts -->
<link href="https://fonts.googleapis.com/css?family=Berkshire+Swash" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Rokkitt" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">

<script>
		function logout() {
			document.getElementById("logoutForm").submit();
		}
</script>
</head>
<body>
<c:url value="/j_spring_security_logout" var="logoutUrl" />

		<nav class="navbar navbar-default navbar-fixed-top">
		  <div class="container">
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>                        
		      </button>
		      <a class="navbar-brand" href="/Quesnr/home">Quesnr</a>
		    </div>
		    <div class="collapse navbar-collapse" id="myNavbar">
		      <ul class="nav navbar-nav navbar-right">
		        <li>
		        	<div class="dropdown">
		        	  <img src="views/resources/images/profilePage.png" alt="Avatar" style="width:40px">
					  <button class="dropbtn">${displayUser.name}</button>
					  <div class="dropdown-content">
					    <a href="settings"><span class="glyphicon glyphicon-cog"></span>&nbsp;Settings</a>
					    <a href="javascript:logout()"><span class="glyphicon glyphicon-off"></span>&nbsp;Log Out</a>
					  </div>
					</div>
		        </li>
		      </ul>		     
		    </div>		     
		  </div>
		  <div class="level-two">
		  	<div class="container-fluid text-center"> 
		  		<div class="row">
		  			<div class="col-sm-12">
		  				<div class="row level-two-links">
		  					<div class="col-sm-2">		  						
		  							<a href="question-paper">
		  							<span class="menus">Question Papers</span>
		  							</a>
		  					</div>
		  					<div class="col-sm-2">		  						
		  							<a href="#">
		  							<span class="menus">MCQ's</span>
		  							</a>
		  					</div>
		  					<div class="col-sm-2">		  						
		  							<a href="#">
		  							<span class="menus">Challenges</span>
		  							</a>
		  					</div>
		  					<div class="col-sm-2">		  						
		  							<a href="#">
		  							<span class="menus">Competitive Adda</span>
		  							</a>
		  					</div>
		  					<div class="col-sm-2">
		  							<a href="profile"><span class="menus">Forum</span></a>
		  					</div>
		  					<div class="col-sm-2">
		  							<a href="profile"><span class="menus">Profile</span></a>
		  					</div>		  					
		  				</div>
		  			</div>
		  		</div>
		  	</div>   	
		  </div>
		</nav>
		<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="hidden"
			name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>