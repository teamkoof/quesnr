<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Quesnr | Account Verification</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="views/resources/css/external-css/bootstrap.min.css">
<script src="views/resources/js/external-js/jquery.min.js"></script>
<script src="views/resources/js/external-js/bootstrap.min.js"></script>
<!-- manual styles -->
<link rel="stylesheet" href="views/resources/css/style.css" type="text/css">
<!-- fonts -->
<link href="https://fonts.googleapis.com/css?family=Berkshire+Swash" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Rokkitt" rel="stylesheet">
<body>
<div class="row content-row">
		<div class="col-sm-7 col-sm-offset-3">
		<div class="content-block">
					<div class="top-content-block">
						<div class="container-fluid text-center ">
							<h3 style="font-family: 'Berkshire Swash', cursive; color:#fff; font-size: 40px;">Quesnr</h3>
							<h4 style="font-family: 'Rokkitt', serif; color:#fff; font-size: 30px;">Account Verification</h4>
						</div>	
				
					</div>
					<div class="middle-content-block after-login" style="border:2px solid #154360; height: 330px;">
						<div class="container-fluid text-center">			
							<br><br>
							<c:choose>
								<c:when test="${empty msg}">
								<h2 style="font-family: 'Rokkitt', serif; color:#E74C3C; font-size: 30px;">Your Account is Verified</h2>				
							</c:when>
							<c:otherwise>
								<h2 style="font-family: 'Rokkitt', serif; color:#E74C3C; font-size: 30px;">Your Account is Already Verified</h2>
							</c:otherwise>
							</c:choose>
							<br>
							<h3 style="font-family: 'Rokkitt', serif; color:#154360; font-size: 25px;">Please Click on the below link to Log-in to Quesnr !</h3>
							<br><br>
							<a href="/Quesnr/sign-in"><button type="button" class="redirect-btn">Login</button></a>
							<br><br>
							<font>Read the <a href="#">Terms and Conditions</a> and <a href="#">Privacy Policy</a></font>	
							</div>	
					</div>
					<div class="container-fluid text-center">
						 <br><br>
						 <font>All rights reserved to <a href="http://localhost:8080/Quesnr/">Quesnr</a></font>
					</div>			   
				</div>
			</div>
		</div>

</body>
</html>