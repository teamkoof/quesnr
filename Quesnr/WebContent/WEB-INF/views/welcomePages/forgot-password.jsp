<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Quesnr</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <link rel="stylesheet" href="views/resources/css/external-css/bootstrap.min.css">
<script src="views/resources/js/external-js/jquery.min.js"></script>
<script src="views/resources/js/external-js/bootstrap.min.js"></script> -->

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bootstrap/3.2.0/css/bootstrap.min.css"/>

<!-- Include FontAwesome CSS if you want to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/fontawesome/4.1.0/css/font-awesome.min.css" />

<!-- BootstrapValidator CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css"/>

<!-- jQuery and Bootstrap JS -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- BootstrapValidator JS -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.bootstrapvalidator/0.5.0/js/bootstrapValidator.min.js"></script>


<script src="views/resources/js/quesnr-js/generic_js.js"></script>
<script src="views/resources/js/quesnr-js/validate_js.js"></script>
<!-- manual styles -->
<link rel="stylesheet" href="views/resources/css/style.css" type="text/css">
<!-- fonts -->
<link href="https://fonts.googleapis.com/css?family=Berkshire+Swash" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Rokkitt" rel="stylesheet">
<title>Forgot Password</title>
<script type="text/javascript">
$(document).ready(function() {
    $('#forgot-password-form').bootstrapValidator({
    	container: 'tooltip',
	    feedbackIcons: {
	    	valid: 'glyphicon glyphicon-ok',
	    	invalid: 'glyphicon glyphicon-remove',
	    	validating: 'glyphicon glyphicon-refresh'
	    },
        fields: {
        	email: {  
                validators: {  
                    notEmpty: {  
                        message: 'The email is required and can\'t be empty'  
                    },
                    emailAddress: {
                        message: 'The email is not valid.'
                    }
                }  
            }
        }
    });
});
</script>

</head>
<body>
<div class="row">
	<div class="col-sm-6 left">
		
	
		<div class="container-fluid text-center login-arena">
			<h2>Quesnr</h2>			
				<c:if test="${msg != null}">
					<div class="alert alert-danger error" id="alert">
						<div class="close" onclick="javascript:closeAlert();">X</div>
						${msg} 
					</div>
				</c:if>				
			<div class="login-block">
				<h3>Login Here !</h3>
				<div class="feilds">
					<form:form action="forgot-password-send-email" method="POST" modelAttribute="user" class="form-horizontal" name="forgot-password-form" id="forgot-password-form">
					
						 <div class="form-group">
						      <div class="col-sm-6 col-sm-offset-3">
						    	<form:input path="email" id="email" class="form-control texters" placeholder="Email"/>
						      </div>
   						 </div>
   						  
   						  <div class="form-group">
						      <div class="col-sm-6 col-sm-offset-3">
						      <!-- raj 18 feb 2018
						      write a javascript -->
						        	<button type="submit"  class="btn btn-default btn-base-design">Send Verification Link</button>
						      </div>
   						 </div>
   						  <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />
					<!-- </form> -->
					</form:form>
					
				</div>
			</div>
		</div>	
	</div>
	
</div>
</body>
</html>