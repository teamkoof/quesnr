<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="org.springframework.security.core.GrantedAuthority"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 
<h1>${errorCode}</h1>
<h1>${errorMessage}</h1>
<br><br>
<h3>Please Click on the below link to Log-in to Quesnr !</h3>
<br><br>
<a href="/Quesnr/sign-in"><button type="button" class="redirect-btn">Login</button></a>	
