<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Quesnr</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <link rel="stylesheet" href="views/resources/css/external-css/bootstrap.min.css">
<script src="views/resources/js/external-js/jquery.min.js"></script>
<script src="views/resources/js/external-js/bootstrap.min.js"></script> -->

<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.js"></script>  
<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>  
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"/>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"> </script>


<script src="views/resources/js/quesnr-js/generic_js.js"></script>
<script src="views/resources/js/quesnr-js/validate_js.js"></script>

<!-- manual styles -->
<link rel="stylesheet" href="views/resources/css/style.css" type="text/css">
<!-- fonts -->
<link href="https://fonts.googleapis.com/css?family=Berkshire+Swash" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Rokkitt" rel="stylesheet">
<title>Forgot Password</title>
<script type="text/javascript">

</script>

</head>
<body>
<div class="row">
	<div class="col-sm-6 left">
		
	
		<div class="container-fluid text-center login-arena">
			<h2>Quesnr</h2>			
				<c:if test="${msg != null}">
					<div class="alert alert-danger error" id="alert">
						<div class="close" onclick="javascript:closeAlert();">X</div>
						${msg} 
					</div>
				</c:if>				
			<div class="login-block">
				<h3>Login Here !</h3>
				<div class="feilds">
					<form:form action="update-password" method="POST" modelAttribute="user" class="form-horizontal" name="reset-password-form" id="reset-password-form">
						<form:hidden path="email"/>
						<form:hidden path="id"/>
						<form:hidden path="role"/>
						
						<div class="form-group">
						      <div class="col-sm-6 col-sm-offset-3">
						    	<form:input path="accessCode" id="accessCode" class="form-control texters" placeholder="Access Code" name="accessCode"/>
						      </div>
   						 </div>
						
						 <div class="form-group">
						      <div class="col-sm-6 col-sm-offset-3">
						    	<form:password path="newPassword" id="newPassword" class="form-control texters" placeholder="New Password" name="newPassword"/>
						      </div>
   						 </div>
   						 <div class="form-group">
						      <div class="col-sm-6 col-sm-offset-3">
						    	<form:password path="reNewPassword" id="reNewPassword" class="form-control texters" placeholder="Confirm Password" name="reNewPassword"/>
						      </div>
   						 </div>
   						  
   						  <div class="form-group">
						      <div class="col-sm-6 col-sm-offset-3">
						      <!-- raj 18 feb 2018
						      write a javascript -->
						        	<button type="submit"  class="btn btn-default btn-base-design" onclick="resetPasswordValidation();">Reset Password</button>
						      </div>
   						 </div>
   						  <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />
					<!-- </form> -->
					</form:form>
					
				</div>
			</div>
		</div>	
	</div>
	
</div>
</body>
</html>