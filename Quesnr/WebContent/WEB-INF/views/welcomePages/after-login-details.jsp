<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="org.springframework.security.core.GrantedAuthority" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Quesnr | Education Details</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <link rel="stylesheet" href="views/resources/css/external-css/bootstrap.min.css">
<script src="views/resources/js/external-js/jquery.min.js"></script>
<script src="views/resources/js/external-js/bootstrap.min.js"></script> -->

<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.js"></script>  
<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>  
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"/>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"> </script>

<script src="views/resources/js/quesnr-js/generic_js.js"></script>
<script src="views/resources/js/quesnr-js/validate_js.js"></script>


<!-- manual styles -->
<link rel="stylesheet" href="views/resources/css/style.css" type="text/css">
<!-- fonts -->
<link href="https://fonts.googleapis.com/css?family=Berkshire+Swash" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Rokkitt" rel="stylesheet">
<script>

$( document ).ready(function() {
	$("#schoolCity").prop("disabled",true);
});

function logout() {
	document.getElementById("logoutForm").submit();
}
</script>
</head>
<body>
<c:url value="/j_spring_security_logout" var="logoutUrl" />
<div class="row" style="  box-shadow: 0 10px 10px -10px #273746;">
	<div class="col-sm-8 col-sm-offset-2">
		<nav class="navbar  reg-nav" >
		  <div class="container-fluid text-center">
		    <div class="navbar-header">
		      <button type="button" class="btn btn-toggle" data-toggle="collapse" data-target="#myNavbar" >
		        <span class="glyphicon glyphicon-menu-hamburger" style="font-size:15px;"></span>                      
		      </button>
		      <a class="navbar-brand header" href="/Quesnr/">
		      	<h2>Quesnr</h2>	
		      </a>
		    </div>
		    <div class="collapse navbar-collapse" id="myNavbar">
		      <ul class="nav navbar-nav navbar-right">
		        <li><button type="button" class="btn btn-default logout-button" onclick="javascript:logout()">Log Out</button></li>
		      </ul>
		    </div>
		  </div>
		</nav>
	</div>
</div>
<div class="empty-row">
<div class="reg-transparency">
	<div class="row content-row">
		<div class="col-sm-7 col-sm-offset-3">
		<div class="content-block">
			<div class="top-content-block">
				<div class="container-fluid text-center ">
					<h3>Welcome ${displayUser.name}</h3>
					<h4>Please Complete Your Profile Before You Proceed</h4>
				</div>	
		
			</div>
			<div class="middle-content-block after-login">
				<div class="container-fluid text-center">			
					<div class="feilds">
					<div class="message">
						${msg} 
					</div>
					<%-- <c:choose>
						<c:when test="${role eq 'student'}">
							<c:url value="studentRegistration" var="registration" />
						</c:when>
						<c:otherwise>
							<c:url value="instituteRegistration" var="registration" />
						</c:otherwise>
					</c:choose> --%>
						<form:form action="complete-registration" method="POST" modelAttribute="user" name="after-login-form" id="after-login-form">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">							    		
	   									<form:select path="student.board.boardId" class="form-control reg-texters required" id="boardId">
	   										<form:option value="0" label="Select Board"/>
										    <form:options items="${boardList}" itemValue="boardId" itemLabel="boardName"  />
										</form:select>
	   								</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
							    		<form:select path="student.studentClass" class="form-control reg-texters required">
											<form:option value="0">Select Class</form:option>
											<form:option value="1" label="Class 1"/>
											<form:option value="2" label="Class 2"/>
											<form:option value="3" label="Class 3"/>
											<form:option value="4" label="Class 4"/>
											<form:option value="5" label="Class 5"/>
											<form:option value="6" label="Class 6"/>
											<form:option value="7" label="Class 7"/>
											<form:option value="8" label="Class 8"/>
											<form:option value="9" label="Class 9"/>
											<form:option value="10" label="Class 10"/>
											<form:option value="11" label="Class 11"/>
											<form:option value="12" label="Class 12"/>
										</form:select>
	   								 </div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<form:input path="student.school" placeholder="School Name"  class="form-control reg-texters"/>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<form:select path="student.schoolState" id="schoolState" class="form-control reg-texters" placeholder="State" onclick="populateIndianCities()">
	   										<form:option value="" label="Select State"/>
											<form:options items="${stateList}" itemValue="stateName" itemLabel="stateName"  />
										</form:select>
										<%-- <form:input path="student.schoolState" placeholder="State" class="form-control reg-texters"/> --%>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<form:select path="student.schoolCity" id="schoolCity" class="form-control reg-texters" placeholder="City" >
	   										<form:option value="" label="Select City"/>
										   	<form:options items="${cityList}" itemValue="cityName" itemLabel="cityName" />
										</form:select>
										<%-- <form:input path="student.schoolCity" placeholder="City"  class="form-control reg-texters"/> --%>
									</div>
								</div>
							</div>	
							<div class="actions">
								<div class="form-group button-block">
									<div class="row">
										<div class="col-sm-6">
											<button type="reset" class="btn btn-default btn-base-design reset-btn">Reset</button>
										</div>
										<div class="col-sm-6">
											<button type="submit" class="btn btn-default btn-base-design reg-btn" onclick="validateAfterLoginDetailsPage();">Register</button>
										</div>
									</div>								    		
		   						</div>
		   					</div>
			
								</form:form>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<%-- <form:form action="complete-registration" method="POST" modelAttribute="user" name="after-login-form" id="after-login-form">
	<form:select path="student.boardId" items="${boardList}" itemValue="boardId" itemLabel="boardName" title="Select Board" ></form:select>
	
	<form:select path="student.studentClass">
		<form:option value="0" label="Select"/>
		<form:option value="1" label="Class 1"/>
		<form:option value="2" label="Class 2"/>
		<form:option value="3" label="Class 3"/>
		<form:option value="4" label="Class 4"/>
		<form:option value="5" label="Class 5"/>
		<form:option value="6" label="Class 6"/>
		<form:option value="7" label="Class 7"/>
		<form:option value="8" label="Class 8"/>
		<form:option value="9" label="Class 9"/>
		<form:option value="10" label="Class 10"/>
		<form:option value="11" label="Class 11"/>
		<form:option value="12" label="Class 12"/>
	</form:select>
	
	<form:input path="student.school" placeholer="School"/>
	<form:input path="student.schoolCity" placeholer="City"/>
	<form:input path="student.schoolState" placeholer="State"/>
	
	<input type="submit" value="submit">
</form:form> --%>
<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="hidden"
			name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>
</body>
</html>