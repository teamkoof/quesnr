<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Quesnr</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 
<link rel="stylesheet" href="views/resources/css/external-css/bootstrap.min.css">
<script src="views/resources/js/external-js/jquery.min.js"></script>
<script src="views/resources/js/external-js/bootstrap.min.js"></script> 
-->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.js"></script>  
<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>  
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"/>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"> </script>


<script src="views/resources/js/quesnr-js/generic_js.js"></script>
<script src="views/resources/js/quesnr-js/validate_js.js"></script>
<!-- manual styles -->
<link rel="stylesheet" href="views/resources/css/style.css" type="text/css">
<!-- fonts -->
<link href="https://fonts.googleapis.com/css?family=Berkshire+Swash" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Rokkitt" rel="stylesheet">
<script>
$(document).ready(function(){
		setInterval(function() { closeAlert(); },5000);
});
	
function openReg(role){
	var selectedRole = role;
	//alert("role : "+selectedRole);
	if(selectedRole == 'student'){
		location.href = "register?role=student";
	}else if(selectedRole == 'institute'){
		location.href = "register?role=institute";
	}

}
function closeAlert(){
	document.getElementById('alert').style.display = 'none';
}
</script>	
</head>
<body>
	<div class="row">
	<div class="col-sm-6 left">
		<div class="row">
	<div class="col-sm-6 col-sm-offset-1">
		<nav class="navbar reg-nav">
		  <div class="container text-center">
		    <div class="navbar-header">
		      <button type="button" class="btn btn-toggle" data-toggle="collapse" data-target="#myNavbar" >
		        <span class="glyphicon glyphicon-menu-hamburger" style="font-size:15px;"></span>                      
		      </button>
		    </div>
		    <div class="collapse navbar-collapse" id="myNavbar">
		      <ul class="nav navbar-nav">
		        <li><a href="#">ABOUT</a></li>
		        <li><a href="#services">HOW IT WORKS</a></li>
		        <li><a href="#portfolio">FAQ's</a></li>
		        <li><a href="#contact">CONTACT</a></li>
		      </ul>
		    </div>
		  </div>
</nav>
	</div>
</div>

	
		<div class="container-fluid text-center login-arena">
			<h2>Quesnr</h2>			
				<c:if test="${msg != null}">
					<div class="alert alert-danger error" id="alert">
						<div class="close" onclick="javascript:closeAlert();">X</div>
						${msg} 
					</div>
				</c:if>	
				<c:if test="${message != null}">
					<div class="alert alert-danger error" id="alert">
						<div class="close" onclick="javascript:closeAlert();">X</div>
						${message} 
					</div>
				</c:if>			
			<div class="login-block">
				<h3>Login Here !</h3>
				<div class="feilds">
					<form:form action="login" method="POST" modelAttribute="user" class="form-horizontal" name="signinform" id="signinform">
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
					<%-- <form action="login"  class="form-horizontal" name="loginForm" method="POST"> --%>
						 <div class="form-group">
						      <div class="col-sm-6 col-sm-offset-3">
						    	<form:input path="username" id="username" class="form-control texters" placeholder="Username" name="username"/>
						       <!--  <input type="text" class="form-control texters" id="username" placeholder="Username" name="username"> -->
						      </div>
   						 </div>
   						  <div class="form-group">
						      <div class="col-sm-6 col-sm-offset-3">
						        <!-- <input type="password" class="form-control texters" id="password" placeholder="Password" name="password" > -->
						        <form:password path="password" class="form-control texters" id="password" placeholder="Password" />
						      </div>
   						 </div>
   						 <div class="form-group">
						      <div class="col-sm-6 col-sm-offset-3">
						       Remember Me: <input type="checkbox" name="remember-me" placeholder="remember-me"/>
						        
						      </div>
   						 </div>
   						  <div class="form-group">
						      <div class="col-sm-6 col-sm-offset-3">
						      <!-- raj 18 feb 2018
						      write a javascript -->
						        	<button type="submit"  class="btn btn-default btn-base-design" onclick="validateLoginPage();">Login</button>
						      </div>
   						 </div>
   						  <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />
					<%-- </form> --%>
					</form:form>
					<div class="forgot-password">
						<p><a href='<c:url value="/forgot-password"></c:url>'>Forgot Password ?</a></p>
					</div>
				</div>
			</div>
		</div>	
	</div>
	<div class="col-sm-6 gate-reg-block" id="gate-reg-block" style="display:block;">
		<div class="right">
			<div class="trasnparency">
				<div class="container-fluid text-center entry-arena">
					<h3>New To <span class="quesnr-color">Quesnr</span>?</h3>
					<div class="reg-entry-block">
					<h3>Enter as..</h3>
					<div class="feilds">
					<form action="/"  class="form-horizontal" name="regEntryForm">
						<div class="form-group">
						      <div class="col-sm-6 col-sm-offset-3">
						        	<button type="button" class="btn btn-default btn-entry-design" id="student-reg" onclick="javascript:openReg('student')">Student</button>
						      </div>
   						 </div>
   						 <div class="form-group">
						      <div class="col-sm-6 col-sm-offset-3">
						        	<button type="button" class="btn btn-default btn-entry-design" id="insti-reg" onclick="javascript:openReg('institute')">Institute</button>
						      </div>
   						 </div>
   					</form>
   					</div>
					</div>
					<h4>To Learn more about Quesnr Roles <a href="#">click here</a></h4>
					<div class="copyrights">
						<p>All rights reserved to <a href="#">Quesnr</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>

</script>

</body>
</html>