<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Quesnr</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <link rel="stylesheet" href="views/resources/css/external-css/bootstrap.min.css">
<script src="views/resources/js/external-js/jquery.min.js"></script>
<script src="views/resources/js/external-js/bootstrap.min.js"></script> -->

<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.js"></script>  
<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>  
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"/>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"> </script>

<script src="views/resources/js/quesnr-js/generic_js.js"></script>
<script src="views/resources/js/quesnr-js/validate_js.js"></script>

<!-- manual styles -->
<link rel="stylesheet" href="views/resources/css/style.css" type="text/css">
<!-- fonts -->
<link href="https://fonts.googleapis.com/css?family=Berkshire+Swash" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Rokkitt" rel="stylesheet">

<script type="text/javascript">
$( document ).ready(function() {
	var role =  $("#role").val();
	 if(role=='student'){
		 $("#studentCity").prop("disabled",true);
		 $("#studentState").prop("disabled",true);
	 }else{
		 $("#instituteCity").prop("disabled",true);
		 $("#instituteState").prop("disabled",true);
	 }
	 
	});

</script>
</head>
<body background="#F4F6F6;">
<div class="row" style="  box-shadow: 0 10px 10px -10px #273746;">
	<input type="hidden" value="${role}" id="role">
	<div class="col-sm-8 col-sm-offset-2">
		<nav class="navbar  reg-nav" >
		  <div class="container-fluid text-center">
		    <div class="navbar-header">
		      <button type="button" class="btn btn-toggle" data-toggle="collapse" data-target="#myNavbar" >
		        <span class="glyphicon glyphicon-menu-hamburger" style="font-size:15px;"></span>                      
		      </button>
		      <a class="navbar-brand header" href="/Quesnr/">
		      	<h2>Quesnr</h2>	
		      </a>
		    </div>
		    <div class="collapse navbar-collapse" id="myNavbar">
		      <ul class="nav navbar-nav navbar-right">
		        <li><a href="#">ABOUT</a></li>
		        <li><a href="#services">HOW IT WORKS</a></li>
		        <li><a href="#portfolio">FAQ's</a></li>
		        <li><a href="#contact">CONTACT</a></li>
		      </ul>
		    </div>
		  </div>
		</nav>
	</div>
</div>
<div class="empty-row">
<div class="reg-transparency">
	<div class="row content-row">
		<div class="col-sm-7 col-sm-offset-3">
		<div class="content-block">
			<div class="top-content-block">
				<div class="container-fluid text-center ">
					<h3>${role} Registration</h3>
					<h4>Get Started With Quesnr - It's Free</h4>
				</div>	
		
			</div>
			<div class="middle-content-block">
				<div class="container-fluid text-center">			
					<div class="feilds">
					<div class="message">
						${msg} 
					</div>
					<c:choose>
						<c:when test="${role eq 'Student'}">
							<c:url value="studentRegistration" var="registration" />
						</c:when>
						<c:otherwise>
							<c:url value="instituteRegistration" var="registration" />
						</c:otherwise>
					</c:choose>
						<form:form action="${registration}" method="POST" modelAttribute="user" name="registrationform" id="registrationform">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
							    		<form:input path="username" id="username" class="form-control reg-texters" placeholder="Username" />
	   								 </div>
	   								 <c:choose>
	   								 	<c:when test="${role eq 'Student'}">
											 <div class="form-group">
							    				<form:input path="student.firstName" class="form-control reg-texters" id="firstname" placeholder="First name"/>
			   								 </div>
			   								 <div class="form-group">
									    		<form:input path="student.email" id="email" class="form-control reg-texters" placeholder="Email" />
			   								 </div>
			   								<div class="form-group">
									    		<form:select path="student.studentAddress.country" id="studentCountry" class="form-control reg-texters" placeholder="Country" onclick="populateStates('student')">
	   												<form:option value="" label="Select Country"/>
										    		<form:options items="${countryList}" itemValue="countryName" itemLabel="countryName"  />
												</form:select>
			   								 </div>
			   								 <div class="form-group">
									    		<form:select path="student.studentAddress.city" id="studentCity" class="form-control reg-texters" placeholder="City" >
	   												<form:option value="" label="Select City"/>
										    		<form:options items="${cityList}" itemValue="cityName" itemLabel="cityName" />
												</form:select>
			   								 </div>
										</c:when>
										<c:otherwise>
											 <div class="form-group">
									    		<form:input path="institute.instituteName" id="instituteName" class="form-control reg-texters" placeholder="Institute Name" />
			   								 </div>
			   								 <div class="form-group">
									    		<form:input path="institute.email" id="email" class="form-control reg-texters" placeholder="Email" />
			   								 </div>
			   								 <div class="form-group">
									    		<form:select path="institute.instituteAddress.country" id="instituteCountry" class="form-control reg-texters" placeholder="Country" onclick="populateStates('institute');">
	   												<form:option value="" label="Select Country"/>
										    		<form:options items="${countryList}" itemValue="countryName" itemLabel="countryName"  />
												</form:select>
			   								 </div>
			   								 <div class="form-group">
									    		<form:select path="institute.instituteAddress.city" id="instituteCity" class="form-control reg-texters" placeholder="City">
	   												<form:option value="" label="Select City"/>
										    		<form:options items="${cityList}" itemValue="cityName" itemLabel="cityName"  />
												</form:select>
			   								 </div>
										</c:otherwise>
	   								 </c:choose>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
							    		<form:password path="password" id="password" class="form-control reg-texters" placeholder="Password" />
	   								 </div>
	   								 <c:choose>
	   								 	<c:when test="${role eq 'Student'}">
	   								 		 <div class="form-group">
									    		<form:input path="student.lastName" id="lastName" class="form-control reg-texters" placeholder="Last Name" />
			   								 </div>
			   								 <div class="form-group">
									    		<form:input path="student.contactNo" id="contactNo" class="form-control reg-texters" placeholder="Phone" />
			   								 </div>
			   								 <div class="form-group">
									    		<form:select path="student.studentAddress.state" id="studentState" class="form-control reg-texters" placeholder="State" onclick="populateCities('student')">
	   												<form:option value="" label="Select State"/>
										    		<form:options items="${stateList}" itemValue="stateName" itemLabel="stateName"  />
												</form:select>
			   								 </div>
			   								 <div class="form-group">
									    		<form:input path="student.studentAddress.zipCode" id="zip" class="form-control reg-texters" placeholder="Zip Code" />
			   								 </div>
	   								 	</c:when>
										<c:otherwise>
										 	<%-- <div class="form-group">
									    		<form:input path="student.lastName" id="lastName" class="form-control reg-texters" placeholder="Last Name" />
			   								 </div> --%>
			   								 <div class="form-group">
									    		<form:input path="institute.contactNo" id="contactNo" class="form-control reg-texters" placeholder="Phone" />
			   								 </div>
			   								 <div class="form-group">
									    		<form:select path="institute.instituteAddress.state" id="instituteState" class="form-control reg-texters" placeholder="State" onclick="populateCities('institute')">
	   												<form:option value="" label="Select State"/>
										    		<form:options items="${stateList}" itemValue="stateName" itemLabel="stateName"  />
												</form:select>
			   								 </div>
			   								 <div class="form-group">
									    		<form:input path="institute.instituteAddress.zipCode" id="zip" class="form-control reg-texters" placeholder="Zip Code" />
			   								 </div>
										</c:otherwise>
	   								 </c:choose>
	   								
								</div>
							</div>	
									 <label class="checkbox-inline container">
									     <form:checkbox path="agreeTerms" id="terms"/><span class="terms">I agree to the <a href="#">Terms and Conditions</a> and <a href="#">Privacy Policy</a></span>
									      <span class="checkmark"></span>
									 </label>
							<div class="actions">
								<div class="form-group button-block">
									<div class="row">
										<div class="col-sm-6">
											<button type="reset" class="btn btn-default btn-base-design reset-btn">Reset</button>
										</div>
										<div class="col-sm-6">
											<button type="submit" class="btn btn-default btn-base-design reg-btn" onclick="validateRegistrationPage();">Register</button>
										</div>
									</div>								    		
		   						</div>
		   					</div>
			
								</form:form>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


</body>
</html>