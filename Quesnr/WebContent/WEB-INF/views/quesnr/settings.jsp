<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="org.springframework.security.core.GrantedAuthority"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script>
	$(document).ready(function(){
	
	var jsonObject = ${userJson};
	var jsonString = JSON.stringify(jsonObject);
	
	//alert("userJson : "+jsonString);
	
	//var json = '{"result":true, "count":42}';
	var obj = JSON.parse(jsonString);
	//alert("username: "+obj.themeColor);
	changeSettings(obj);
	
	});
	
</script>
<div class="container-fluid text-center">
	<div class="row">
		<div class="col-sm-3">
			<div class="pic-holder">
				<img src="views/resources/images/profilePage.png" class="img-circle profileImage"
				alt="${displayUser.name}" id="profileImage" width="240" height="230">			
			</div>
			<div class="front-line-details-holder">
				<span class="name"><font>${displayUser.name}</font></span><br>	
				<div class="role"><font>${role}</font></div>			
				<div class="container-fluid text-center base1 ">				
				  <table class="table">				  
				    <tbody>
				      <tr>
				        <td><span class="glyphicon glyphicon-envelope"></span></td>
				         <td><span id="td-email">${displayUser.email}</span>				       
				        </td>				       
				      </tr>
				     <tr>
				        <td><span class="glyphicon glyphicon-phone"></span></td>
				        <td><span id="td-contact">${displayUser.contactNo}</span>			        
				      </tr>
				       <tr>
				        <td><span class="glyphicon glyphicon-home"></span></td>
				        <td><span class="address"><font>${user.student.studentAddress.city}, ${user.student.studentAddress.state}, ${user.student.studentAddress.country}</font></span></td>
				      </tr>
				    </tbody>
				  </table>
				</div>				
			</div>
			
		</div>
		<div class="col-sm-9 right-side">
				<form:form action="changeSettings" >
				<div class="container-fluid text-left details">		
						<div class="header">
					    	<div class="row">
					    		<div class="col-sm-3">
					    			<h3>Settings</h3>
					    		</div>				    		
					    		<div class="col-sm-9">
					    			<span class="update" style="float:right;"><button type="submit" class="btn btn-default btn-lg-edit" id="btn-lg-edit" >Save</button></span>
					    		</div>
					    	</div>
					    	</div>
					    	<hr>			     	
				     		<div class="row fieldsholder">
				     			<div class="col-sm-3">
				     			
				     			</div>
				     			<div class="col-sm-9">
				     			
				     			</div>
				     		</div>			 
				</div>
				</form:form>
		</div>
	</div>
</div>
