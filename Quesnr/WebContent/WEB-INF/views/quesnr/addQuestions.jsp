<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="org.springframework.security.core.GrantedAuthority"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="_csrf" content="${_csrf.token}" />
<!-- default header name is X-CSRF-TOKEN -->
<meta name="_csrf_header" content="${_csrf.headerName}" />
<style>
.question-div {
	margin-right: 1%;
	margin-left: 1%;
	margin-top: 9%;
	border: 1px solid #000;
	height: 100%;
	padding-top: 2%;
}

textarea {
	resize: none;
}

.questionArea {
	border: 2px solid #000;
}
</style>
<script>
	var i;
	var j;
	var questionIndex = 0;
	var mainIndex = 0;
	var removeCount = 0;
	var removedIndex = [];
	$(document).ready(function() {
		addQuestion();
	});
	function addQuestion() {
		questionIndex++;
		mainIndex++;
		i = 0;
		j = 0;
		//----------------------------------------------------------------------------//
		var question = $("<div id='questionArea"+questionIndex+"' class='questionArea'><div class='container-fluid'><div class='row'>"
				+ "<textarea class='form-control getAll' rows='1' id='question"+questionIndex+"' style='position:relative;' name='question' placeholder='Please enter your question here...'></textarea></div>"
				+ "<br><div class='row'><div class='col-sm-4'><select id='level"+questionIndex+"' name='level' class='form-control getAll'>"
				+ "<option data-hidden='true' selected='selected'>Difficulty</option><option>Easy</option>"
				+ "<option>Medium</option><option>Hard</option></select></div><div class='col-sm-4'>"
				+ "<input id='mark"+questionIndex+"' name='mark' class='form-control getAll' placeholder='Marks' ></input></div>"
				+ "<div class='col-sm-4'><input id='time"
				+ questionIndex
				+ "' name='time' class='form-control getAll' placeholder='Time(minutes)'></input></div></div><br><div class='row'><div class='col-sm-4'>"
				+ "<select id='year"+questionIndex+"' name='year' class='form-control getAll'	title='Academic year'>"
				+ "<option data-hidden='true' selected='selected'>Academic	year</option>"
				+ "<c:forEach var='YearList' items='${AcademicYearsList}'><option>${YearList.year}</option></c:forEach></select></div><div class='col-sm-4'><div id='questionType'>"
				+ "<select id='type"
				+ questionIndex
				+ "' name='type"
				+ questionIndex
				+ "' class='form-control ansType' onchange='addAnswer("
				+ questionIndex
				+ ")'>"
				+ "<option data-hidden='true' selected='selected'>Question	type</option>"
				+ "<c:forEach var='TypeList' items='${QuestionTypeList}'><option value='${TypeList.typeId}'>${TypeList.type}</option>"
				+ "</c:forEach></select></div></div><div class='col-sm-4'><div id='imageBox"+questionIndex+"'>"
				+ "<label class='checkbox-inline'><input type='checkbox'"
				+ "id='containsImage"
				+ questionIndex
				+ "' value='yes' onchange='addImageButton(this,"
				+ questionIndex
				+ ")'>Contains Images ?</label>"
				+ "</div></div></div><br><div class='row'><div id='answerArea"+questionIndex+"'></div></div><br>"
				+ "<div class='row text-center'><button class='btn btn-success' type='button' id='removeQuestionbtn'"
				+ "onclick='removeQuestion("+questionIndex+")'>Remove</button></div></div></div>");

		$("#questionRoot").append(question);
		document.getElementById("removeQuestionbtn").disabled = false;
		//----------------------------------------------------------------------------//		
	}
	
	function removeQuestion(index){		
		if(mainIndex!=1){
			$("#questionArea"+index).remove();
			removedIndex[removeCount] = index;
			removeCount++;
			mainIndex--;
		}			
		else
		{			
			document.getElementById("removeQuestionbtn").disabled = true;
		}
	}
	
	function addImageButton(imageCheckBox, index) {
		if (imageCheckBox.checked) {
			var imgbtn = $("<input type='file' id='image"+index+"' name='image'>");
			$("#imageBox" + index).append(imgbtn);
		} else {
			$("#image" + index).remove();
		}
	}

	function addAnswer(answerIndex) {
		var index = $("#type" + answerIndex).prop('selectedIndex');
		if (index == 0) {
			$("#answerArea" + answerIndex).html('');
		} else if (index == 1) {
			$("#answerArea" + answerIndex).html('');
			var answerItem = $("<div class='container-fluid'>Please select check mark infront of correct answer.<div class='text-center' id='answer"+answerIndex+"'></div></div>");
			$("#answerArea" + answerIndex).append(answerItem);
			j = 1;
			var row = "<div class='row' id='row"+answerIndex+""+j+"'></div>";
			$("#answer" + answerIndex).append(row);
			for (i = 1; i <= 4; i++) {
				var answerItem = $("<div class='col-sm-4' id='col"
						+ answerIndex
						+ ""
						+ i
						+ "' style='padding-top:2%'><div class='input-group'><span class='input-group-addon' id='basic-addon1"+answerIndex+"'><input type='checkbox' name='answerSelected"+answerIndex+"'></span><input class='form-control getAll' id='choise"+i+"' name='choise"+answerIndex+"' placeholder='Enter option'></div></div>");
				$("#row" + answerIndex + "" + j).append(answerItem);
				if (i % 3 == 0) {
					j++;
					var row = "<div class='row' id='row"+answerIndex+""+j+"'></div>";
					$("#answer" + answerIndex).append(row);
				}
			}
			var addOptionButton = "<br><div class='container-fluid'><div class='row text-center'><button type='button' class='btn btn-success' onclick='addMoreOptions("
					+ answerIndex
					+ ")'>Add More +</button>"
					+ "<button type='button' id='removebtn"
					+ answerIndex
					+ "' class='btn btn-success' onclick='removeOption("
					+ answerIndex + ")'>Remove</button></div></div>";
			$("#answerArea" + answerIndex).append(addOptionButton);

		} else if (index == 2) {
			$("#answerArea" + answerIndex).html('');
			var answerItem = $("<textarea class='form-control getAll' rows='4' id='answer"+answerIndex+"' name='answer'" +
			"placeholder='Please enter your answer here...'/>");
			$("#answerArea" + answerIndex).append(answerItem);
		} else if (index == 3) {
			$("#answerArea" + answerIndex).html('');
			var answerItem = $("<textarea class='form-control getAll' rows='1' id='answer"+answerIndex+"' name='answer'" +
				"placeholder='Please enter your answer here...'/>");
			$("#answerArea" + answerIndex).append(answerItem);
		} else if (index == 4) {
			$("#answerArea" + answerIndex).html('');
			var answerItem = '<div id="answer"><input class="getAll" id="answer'+answerIndex+'" type="radio" checked="checked" name="radio'+answerIndex+'" value="true">True'
					+ '<input class="getAll" id="answer'+answerIndex+'" type="radio" name="radio'+answerIndex+'" value="false">False</div>';
			$("#answerArea" + answerIndex).append(answerItem);
		}
	}
	function addMoreOptions(index) {
		var answerItem = "<div class='col-sm-4' id='col"
				+ index
				+ ""
				+ i
				+ "'  style='padding-top:2%'><div class='input-group'><span class='input-group-addon' id='basic-addon1"+index+"'><input type='checkbox' name='answerSelected"+index+"'></span><input class='form-control getAll' id='choise"+i+"' name='choise"+index+"' placeholder='Enter option '></div></div>";
		$("#row" + index + "" + j).append(answerItem);
		if (i % 3 == 0) {
			j++;
			var row = "<div class='row' id='row"+index+""+j+"'></div>";
			$("#answer" + index).append(row);
		}
		i = i + 1;
		$("#removebtn" + index).prop("disabled", false);
	}
	function removeOption(index) {
		if (i == 4) {
			$("#removebtn" + index).prop("disabled", true);
			$("#row" + index + "" + j).remove();
			j--;
			return;
		}
		i--;
		$("#col" + index + "" + i).remove();
		if (i % 3 == 0) {
			$("#row" + index + "" + j).remove();
			j--;
		}
	}
	
	function validateIndex(index){
		//alert("size : "+removedIndex.length);
		for(c = 0;c<removedIndex.length;c++){
			if(index == removedIndex[c]){
				//alert("Found");
				return false;	
			}				
		}
		return true;
	}
	function submitQuestions() {
		var questionArray = [];
		//alert(removedIndex+" - "+mainIndex);
		var rCount = 0;
		for (i = 1; i <= questionIndex; i++) {
		if(validateIndex(i)){
			//alert("Inside");
			var question = {};
			question["question"] = $("#question" + i).val();
			question["level"] = $("#level" + i).val();
			question["mark"] = $("#mark" + i).val();
			question["time"] = $("#time" + i).val();
			question["year"] = $("#year" + i).val();
			if ($("#containsImage" + i).is(":checked")) {
				question["image"] = "true";
			} else
				question["image"] = "false";

			var questionType = {};
			questionType["typeId"] = $("#type" + i).val();
			questionType["type"] = $("#type" + i + " option:selected").html();

			var answer = {};
			var option = [];
			var answerList = [];

			var index = $("#type" + i).prop('selectedIndex');
			var innerIndex = 0;
			if (index == 1) {
				var MCQarray = $("[name='choise" + i + "']").serializeArray();
				for (k = 0; k < MCQarray.length; k++) {
					var optionArray = {};
					optionArray["optionName"] = MCQarray[k].value;
					option[k] = optionArray;
				}

				var inputgrp;
				$("#answer" + i)
						.find(".input-group")
						.each(
								function() {									
									inputgrp = $(this);
									$(this)
											.find(
													"[name='answerSelected" + i
															+ "']:checked")
											.each(
													function() {
														
														var option = inputgrp
																.find(
																		"[name='choise"
																				+ i
																				+ "']")
																.val();
														
														var answerArray = {};
														answerArray["answer"] = option
														answerList[innerIndex] = answerArray;
														innerIndex++;
													});
								});

		
			} else {
				answer["answer"] = $("#answer" + i).val();
				answerList[innerIndex] = answer;
			}

			var rootModel = {};
			rootModel["questionType"] = questionType;
			rootModel["question"] = question;
			rootModel["mcqOptions"] = option;
			rootModel["answers"] = answerList;

				rootModel["boardId"] = $("#boardId option:selected").val();			
				rootModel["classId"] = $("#classId option:selected").val();
				rootModel["subjectId"] = $("#subjectId option:selected").val();
				rootModel["chapterId"] = $("#chapterId option:selected").val();
			//alert("I : "+(i-1));
			questionArray[rCount] = rootModel;	
			rCount++;
			}
		}
		alert(JSON.stringify(questionArray));
		var header = $("meta[name='_csrf_header']").attr("content");
		var token = $("meta[name='_csrf']").attr("content");

		$.ajax({
			url : "addQuestion",
			type : "post",
			beforeSend : function(xhr) {
				xhr.setRequestHeader(header, token);
			},
			contentType : "application/json",
			data : JSON.stringify(questionArray),
			success : function(response) {
				alert("Questions added successfully.");			
			},
			error : function(response) {
				alert("Error : " + JSON.stringify(response));
			}
		});	
	}
	function resetAll() {
		location.reload();
	}

	$(document.body).on('change', '#classId', function() {
		var header = $("meta[name='_csrf_header']").attr("content");
		var token = $("meta[name='_csrf']").attr("content");
		var boardId = $("#boardId option:selected").val();
		var classId = $(this).val();
		$.ajax({
			url : "getClassesByBoardId",
			type : "get",
			beforeSend : function(xhr) {
				xhr.setRequestHeader(header, token);
			},
			contentType : "application/json",
			data : {boardId:boardId,classId:classId},
			success : function(response) {
				$("#subjectId").html('');
				$("#subjectId").append("<option value='0'>Select Subject</option>");
				$.each(response,function(index){
					var option = "<option value="+response[index].subjectId+">"+response[index].subjectName+"</option>";
					$("#subjectId").append(option);
				});
			},
			error : function(response) {
				alert("Error : " + JSON.stringify(response));
			}
		});
	});
	
	$(document.body).on("change","#subjectId",function(){
		var header = $("meta[name='_csrf_header']").attr("content");
		var token = $("meta[name='_csrf']").attr("content");
		var subjectId = $("#subjectId option:selected").val();
		//alert(subjectId);
		$.ajax({
			url : "getChapterListBySubjectId",
			type : "get",
			beforeSend : function(xhr) {
				xhr.setRequestHeader(header, token);
			},
			data : {subjectId : subjectId},
			contentType : "application/json",
			success : function(response){
				$("#chapterId").html('');
				$("#chapterId").append("<option value='0'>Select Chapter</option>");
				$.each(response,function(index){
					var option = "<option value="+response[index].chapterId+">"+response[index].chapterName+"</option>";
					$("#chapterId").append(option);
				});
			},
			error : function(response){
				alert("Error : "+JSON.stringify(response));
			}
		});
	});
</script>
<div class="question-div">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<form id="questionForm">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
				<div id="questionRoot">
					<div class="row" style="margin-bottom: 3%">
						<div class="col-sm-3">
							<select id="boardId" class="form-control">
								<option value="0">Select Board</option>
								<c:forEach var="boardList" items="${boardList}">
									<option value="${boardList.boardId}">${boardList.boardName}</option>
								</c:forEach>
							</select>
						</div>
						<div class="col-sm-3">
							<select id="classId" class="form-control">
								<option>Select Class</option>
								<c:forEach var="classList" items="${classList}">
									<option value="${classList.classNum}">${classList.classChar}</option>
								</c:forEach>
							</select>
						</div>
						<div class="col-sm-3">
							<select id="subjectId" class="form-control">
								<option value="0">Select Subject</option>
							</select>
						</div>
						<div class="col-sm-3">
							<select id="chapterId" class="form-control">
								<option value="0">Select Chapter</option>
							</select>
						</div>
					</div>
				</div>
				<div class="container-fluid" style="margin-top: 5%">
					<div class="row text-center">
						<button class="btn btn-success pull-left" type="button"
							onclick="submitQuestions()">Submit</button>
						<button class="btn btn-success" type="button" onclick="resetAll()">Reset
							All</button>
						<button class="btn btn-success pull-right" type="button"
							onclick="addQuestion()">Add Question</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>