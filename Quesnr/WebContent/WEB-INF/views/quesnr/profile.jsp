<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="org.springframework.security.core.GrantedAuthority"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script>
/* $(document).ready(function(){
	setInterval(function() { closeAlert(); },5000);
	}); */

function changeToText(editValue) {
	var value = editValue;
//	alert("value : "+value);
	if(value == 'edit-email'){ 
		    $("#td-email").hide();
		    $("#i-email").show();
		    $("#edit-email").hide();
		    $("#close-edit-email").show();
	}
	else if(value == 'edit-contact'){	
		    $("#td-contact").hide();		    
	        $("#i-contact").show();	  
	        $("#edit-contact").hide();	   
	        $("#close-edit-contact").show();
	}
	else if(value == 'close-edit-email'){
		$("#td-email").show();		    
        $("#i-email").hide();	  
        $("#edit-email").show();	   
        $("#close-edit-email").hide();
	}
	else if(value == 'close-edit-contact'){
		$("#td-contact").show();		    
        $("#i-contact").hide();	  
        $("#edit-contact").show();	   
        $("#close-edit-contact").hide();
	}
   
   $('#btn-sm-edit').css({'display':'block', 'margin-left':'35%', 'margin-top':'-7%'});
	
}
	/* 	do it in ajax */
	function changePic() {
		alert("files :"+document.getElementById("ppic").files[0].name);
		 var formData = new FormData();
		 formData.append("file",document.getElementById("ppic").files[0]);
		 alert("formdata  : "+formData.file); 
		 
		 //var myForm = document.getElementById("fileForm");

	        $.ajax({
	            type:'POST',
	            url:'changeProfilePic',
	            data: formData,
	            async:false,
	            processData: false,
	            contentType: false,
	            enctype: 'multipart/form-data',
	            success:function(response){
	                profilePicContainer.html('');
	            	 var img = '<img src="data:' + data.contenttype + ';base64,'
	                  + data.base64 + '"/>';
	    
	                  profilePicContainer.append(img);
	                  
	                if(response.Result == "success"){
	                    alert("success : "+JSON.stringify(response));
	                } 
	            	alert('Done');
	            },
	            error:function(response){
	                alert("Error : "+JSON.stringify(response));
	                profilePicContainer.html('');
	            	 var img = '<img src="data:' + data.contenttype + ';base64,'
	                  + data.base64 + '"/>';
	    				alert(img);
	                  profilePicContainer.append(img);
	                  
	                if(response.Result == "success"){
	                    alert("success : "+JSON.stringify(response));
	                } 
	            	alert('Could not Update Picture');
	            }
	        });
	}
	
	function closeAlert(){
		document.getElementById('alert').style.display = 'none';
	}
</script>

<div class="container-fluid text-center">
	<div class="row">
		<div class="col-sm-3">
			<div class="pic-holder">
			<img src="views/resources/images/profilePage.png" class="img-circle profileImage"
			alt="${displayUser.name}" onmouseover="hoverHandler();" id="profileImage" width="240" height="230">
				<div class="pic-transparency"> 
					 <div class="link">
						<button type="submit"
						class="btn btn-default profile-btn" id="profileButton" data-toggle="modal" data-target="#uploadPicModal">Change Picture</button>			
					  </div>
				</div> 
			</div>
			<div class="front-line-details-holder">
				<span class="name"><font>${displayUser.name}</font></span><br>	
				<div class="role"><font>${role}</font></div>			
				<div class="container-fluid text-center base1 ">	
				<form:form action="update-profile" method="POST" modelAttribute="user" name="profileUpdateform" id="profileUpdateform">
				<form:hidden path="userId"></form:hidden>				
				  <table class="table">				  
				    <tbody>
				      <tr>
				        <td><span class="glyphicon glyphicon-envelope"></span></td>
				        <td><span id="td-email">${displayUser.email}</span>
				        <form:input path="student.email" id="i-email" style="display:none; width:200px;"></form:input>
				        </td>
				        <td id="edit-base1" >
				        <span class="glyphicon glyphicon-pencil" id="edit-email" title="Edit" onclick="javascript:changeToText('edit-email');" style="cursor:pointer;"></span>
				        <span class="glyphicon glyphicon-remove" id="close-edit-email" title="Close" onclick="javascript:changeToText('close-edit-email');" style="display:none; cursor:pointer;"></span>
				        </td>
				      </tr>
				     <tr>
				        <td><span class="glyphicon glyphicon-phone"></span></td>
				        <td><span id="td-contact">${displayUser.contactNo}</span>
				         <form:input path="student.contactNo" id="i-contact" style="display:none;  width:200px;"></form:input></td>
				         <td id="edit-base1">
				         <span class="glyphicon glyphicon-pencil" id="edit-contact" title="Edit" onclick="javascript:changeToText('edit-contact');" style="cursor:pointer;"></span>
				         <span class="glyphicon glyphicon-remove" id="close-edit-contact" title="Close" onclick="javascript:changeToText('close-edit-contact');" style="display:none; cursor:pointer;"></span>				         </td>
				      </tr>
				       <tr>
				        <td><span class="glyphicon glyphicon-home"></span></td>
				        <td><span class="address"><font>${user.student.studentAddress.city}, ${user.student.studentAddress.state}, ${user.student.studentAddress.country}</font></span></td>
				      </tr>
				    </tbody>
				  </table>
				  <button type="submit" class="btn btn-default btn-sm-edit" id="btn-sm-edit" style="display:none;" onclick="UpdateContactDetails();">Update</button>
				  </form:form>
				</div>				
			</div>
			
		</div>
		<div class="col-sm-9 right-side">
				<div class="container-fluid text-left details">					 
					  <ul class="nav nav-pills">
					    <li class="pills active"><a data-toggle="pill" href="#basic-details">Account</a></li>
					    <li class="pills" style="width:25%;"><a data-toggle="pill" href="#eduDetails" style="padding-left:30px;">Education Details</a></li>
					    <li class="pills"><a data-toggle="pill" href="#history" style="padding-left:75px;">History</a></li>
					  </ul>
					  
					  <div class="tab-content">
					    <div id="basic-details" class="tab-pane fade in active">
					    <form:form method="POST" action="update-Profile-password" modelAttribute="user" name="updateAccountDetailsForm" id="updateAccountDetailsForm">
					    <form:hidden path="userId"></form:hidden>		
					    	<div class="header">
					    	<div class="row">
					    		<div class="col-sm-3">
					    			<h3>Account</h3>
					    		</div>
					    		<div class="col-sm-5">					    			
										<c:if test="${updateMsg != null}">
									       <div class="alert alert-success mesgBlock" id="alert">
												<div class="close" onclick="javascript:closeAlert();" style="font-size:15px;">X</div>
												${updateMsg} 
											</div>
										</c:if>
										<c:if test="${errorUpdateMsg != null}">
									       <div class="alert alert-danger mesgBlock" id="alert">
												<div class="close" onclick="javascript:closeAlert();" style="font-size:15px;">X</div>
												${errorUpdateMsg} 
											</div>
										</c:if>
					    		</div>
					    		<div class="col-sm-4">
					    			<span class="update" style="float:right;"><button type="submit" class="btn btn-default btn-lg-edit" id="btn-lg-edit-1" onclick="updateAccountDetails();">Update</button></span>
					    		</div>
					    	</div>
					    	</div>
					    	<hr>			     	
					     		<div class="row fieldsholder">
					     			<div class="col-sm-3">
					     				<form:label path="username" class="prof-label">Username</form:label>
					     			</div>
					     			<div class="col-sm-9">
					     				<form:input path="username" id="username" class="form-control prof-text" readonly="true"></form:input>
					     			</div>
					     		</div>		
					     		<div class="row fieldsholder">
					     			<div class="col-sm-3">
					     				<form:label path="password" class="prof-label">Change Password</form:label>
					     			</div>
					     			<div class="col-sm-9">
					     				<form:password path="curPassword" id="password" placeholder="Current Password" class="form-control prof-text"></form:password><br>
					     				<form:password path="newPassword" id="newPassword" placeholder="New Password" class="form-control prof-text"></form:password><br>
					     				<form:password path="reNewPassword" id="reNewPassword" placeholder="Re-Type New Password" class="form-control prof-text"></form:password>
					     			</div>
					     		</div>	
					     						     			     		
					     	</form:form>
					    </div>
					    <div id="eduDetails" class="tab-pane fade">
					       <form:form method="POST" action="update-profile" modelAttribute="user" name="updateEducationalDetailsForm" id="updateEducationalDetailsForm">
					    	<form:hidden path="userId"></form:hidden>		
					    	<div class="header">
					    	<div class="row">
					    		<div class="col-sm-4">
					    			<h3>Educational Details</h3>
					    		</div>
					    		<div class="col-sm-8">
					    			<span class="update" style="float:right;"><button type="submit" class="btn btn-default btn-lg-edit" id="btn-lg-edit" onclick="updateEducationalDetails();">Update</button></span>
					    		</div>
					    	</div>
					    	</div>
					    	<hr>			     	
					     		<div class="row fieldsholder">
					     			<div class="col-sm-3">
					     				<form:label path="student.board.boardId" class="prof-label">Board Name</form:label>
					     			</div>
					     			<div class="col-sm-9">
					     				<form:select path="student.board.boardId" class="form-control prof-text" items="${boardList}"  itemValue="boardId" itemLabel="boardName" >
										</form:select>
					     			</div>
					     		</div>		
					     		<div class="row fieldsholder">
					     			<div class="col-sm-3">
					     				<form:label path="student.studentClass" class="prof-label">Class</form:label>
					     			</div>
					     			<div class="col-sm-9">
					     				<form:select path="student.studentClass" class="form-control prof-text">
											<form:option value="0" label="Select Class"/>
											<form:option value="1" label="Class 1"/>
											<form:option value="2" label="Class 2"/>
											<form:option value="3" label="Class 3"/>
											<form:option value="4" label="Class 4"/>
											<form:option value="5" label="Class 5"/>
											<form:option value="6" label="Class 6"/>
											<form:option value="7" label="Class 7"/>
											<form:option value="8" label="Class 8"/>
											<form:option value="9" label="Class 9"/>
											<form:option value="10" label="Class 10"/>
											<form:option value="11" label="Class 11"/>
											<form:option value="12" label="Class 12"/>
										</form:select>
					     			</div>
					     		</div>	
					     		<div class="row fieldsholder">
					     			<div class="col-sm-3">
					     				<form:label path="student.school" class="prof-label">School</form:label>
					     			</div>
					     			<div class="col-sm-9">
					     				<form:input path="student.school" id="school" class="form-control prof-text"></form:input>
					     			</div>
					     		</div>		
					     		<div class="row fieldsholder">
					     			<div class="col-sm-3">
					     				<form:label path="student.schoolCity" class="prof-label">School City</form:label>
					     			</div>
					     			<div class="col-sm-9">
					     				<form:select path="student.schoolCity" id="schoolCity" class="form-control prof-text" placeholder="City" >
	   										<form:option value="" label="Select City"/>
										   	<form:options items="${cityList}" itemValue="cityName" itemLabel="cityName" />
										</form:select>
					     				<%-- <form:input path="student.schoolCity" id="schoolCity" class="form-control prof-text"></form:input> --%>
					     			</div>
					     		</div>		
					     		<div class="row fieldsholder">
					     			<div class="col-sm-3">
					     				<form:label path="student.schoolState" class="prof-label">School State</form:label>
					     			</div>
					     			<div class="col-sm-9">
					     				<form:select path="student.schoolState" id="schoolState" class="form-control prof-text" placeholder="State" onclick="populateIndianCities()">
	   										<form:option value="" label="Select State"/>
											<form:options items="${stateList}" itemValue="stateName" itemLabel="stateName"  />
										</form:select>
					     				<%-- <form:input path="student.schoolState" id="schoolState" class="form-control prof-text"></form:input> --%>
					     			</div>
					     		</div>		
					     						     			     		
					     	</form:form>
					    </div>
					    <div id="history" class="tab-pane fade history">
					      <div class="header">
					    	<div class="row">
					    		<div class="col-sm-4">
					    			<h3>History</h3>
					    		</div>					    		
					    	</div>
					    	</div>
					      <hr>
					      <div class="row fieldsholder">
					     			<div class="col-sm-3">
					     				<font>Quesnr Joining Date</font> 
					     			</div>
					     			<div class="col-sm-9">					     			
					     			 	<fmt:formatDate pattern="EEE, MMM dd, yyyy" value="${user.dateCreated}" var="dateCreation"/>		
					     			 	<input type="text" name="d" value="<c:out value="${dateCreation}"/>" readonly="true" size="10" class="form-control prof-text"/>			     						     				
					     			</div>
					      </div>	
					       <div class="row fieldsholder">
					     			<div class="col-sm-3">
					     				<font>Last Log-in</font> 
					     			</div>
					     			<div class="col-sm-9">					     			
					     			 	<fmt:formatDate pattern="EEE, MMM dd, yyyy" value="${user.lastLoggedInTime}" var="lastLogin"/>		
					     			 	<input type="text" name="d" value="<c:out value="${lastLogin}"/>" readonly="true" size="10" class="form-control prof-text"/>			     						     				
					     			</div>
					      </div>		
					      <div class="row fieldsholder">
					     			<div class="col-sm-3">
					     				<font>Your visit count</font> 
					     			</div>
					     			<div class="col-sm-9">					     				
					     			 	<font>${user.loginCount} times.</font>			     						     				
					     			</div>
					      </div>
					      	
					      <p></p>
					    </div>
					  </div>
					</div>
		</div>
	</div>
</div>
<!-- Modal -->
  <div class="modal fade" id="uploadPicModal" role="dialog">
    <div class="modal-dialog modal-lg">
     <%-- <form:form id="upload-pic" class="upload-box" action="changeProfilePic" modelAttribute="user" method="POST" enctype="multipart/form-data"> --%>
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload Picture</h4>
        </div>
        <div class="modal-body">
        	<div class="row">
        		<div class="col-sm-5">
        		<!-- 	<input type="file" id="file" name="profilePic" />    		 -->
					 <form id="fileForm">
					    <input type="file" name="file" id="ppic" />
					    <button id="btnUpload" type="button" onclick="javascript:changePic();">Upload file</button>
					</form>
        		</div>
        		<div class="col-sm-7">
        			 <div class="profilePicContainer" id="profilePicContainer"></div>
        		</div>
        	</div>		    
        </div>
        <div class="modal-footer">
          <!-- <button type="submit" class="btn btn-default btn-pic" onclick="javascript:changePic();">Upload</button> -->
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
     <%--  </form:form> --%>
     
    </div>
  </div>