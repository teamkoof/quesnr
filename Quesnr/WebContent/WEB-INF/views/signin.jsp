<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page session="false"%>
<!DOCTYPE html>
<html>
<head>

</head>
<body>
	<form:form action="login" method="POST" modelAttribute="user" name="signinform" id="signinform">
	
		<table border="1">
			<tr>
				<td><form:label path="username"><spring:message code="user.username"/></form:label></td>
				<td><form:input path="username" placeholder="Enter Username" /></td>
			</tr>
			<tr>
				<td><form:label path="password"><spring:message code="user.password"/></form:label></td>
				<td><form:password path="password" placeholder="Enter Password"/></td>
			</tr>
		   	<tr>
		   		<td><a href=""><input type="submit" value="Login"></a></td>
		   		<td></td>
		   	</tr>
		   	<tr>
		   		<td><a href="<c:url value="/signup" />">Register Now!</a></td>
		   		<td></td>
		   	</tr>
  		</table> 
	</form:form>
</body>
</html>