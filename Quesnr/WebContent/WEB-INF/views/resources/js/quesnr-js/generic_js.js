function changeSettings(jsonUserObj){
	//alert("in change settings for user : "+jsonUserObj.username);
	
	//$(".navbar").css("background-color",jsonUserObj.themeColor );
}

function populateStates(role){
	if(role=='student'){
		if($('#studentCountry').val() == ""){
			$("#studentCity").prop("disabled",true);
			$("#studentState").prop("disabled",true);
		}else{
			$("#studentState").prop("disabled",false);
			var country = $("#studentCountry").val();
			$.ajax({
				type : "GET",
				contentType : "application/json;charset=utf-8",
				data : {'country': country},
				url : "states",
				dataType : 'json',
				success : function(response) {
					$("#studentState").empty();
					$("<option/>").attr("value","").text("Select State").appendTo($("#studentState"));
					for(var i=0;i<response.length;i++){
						$("<option/>").attr("value",response[i].stateName).text(response[i].stateName).appendTo($("#studentState"));
					}
				},
				error : function(e) {
					$("#studentState").empty();
					$("<option/>").attr("value","").text("Select State").appendTo($("#studentState"));
					//alert('error');
				}
			}); 
			 
		}
	}else{
		if( $('#instituteCountry').val() == ""){
			 $("#instituteCity").prop("disabled",true);
			 $("#instituteState").prop("disabled",true);
		}else{
			 $("#instituteState").prop("disabled",false);
			 
				var country = $("#instituteCountry").val();
				$.ajax({
					type : "GET",
					contentType : "application/json;charset=utf-8",
					data : {'country': country},
					url : "states",
					dataType : 'json',
					success : function(response) {
						$("#instituteState").empty();
						$("<option/>").attr("value","").text("Select State").appendTo($("#instituteState"));
						for(var i=0;i<response.length;i++){
							$("<option/>").attr("value",response[i].stateName).text(response[i].stateName).appendTo($("#instituteState"));
						}
					},
					error : function(e) {
						$("#instituteState").empty();
						$("<option/>").attr("value","").text("Select State").appendTo($("#instituteState"));
						//alert('error');
					}
				}); 
				 
			 
		}
		 
	}
}

function populateCities(role){
	if(role=='student'){
		if($('#studentState').val() == ""){
			$("#studentCity").prop("disabled",true);
		}else{
			$("#studentCity").prop("disabled",false);
			var state = $("#studentState").val();
			$.ajax({
				type : "GET",
				contentType : "application/json;charset=utf-8",
				data : {'state': state},
				url : "cities",
				dataType : 'json',
				success : function(response) {
					$("#studentCity").empty();
					$("<option/>").attr("value","").text("Select City").appendTo($("#studentCity"));
					for(var i=0;i<response.length;i++){
						$("<option/>").attr("value",response[i].cityName).text(response[i].cityName).appendTo($("#studentCity"));
					}
				},
				error : function(e) {
					$("#studentCity").empty();
					$("<option/>").attr("value","").text("Select City").appendTo($("#studentCity"));
					//alert('error');
				}
			}); 
			 
		}
	}else{
		if( $('#instituteState').val() == "0"){
			$("#instituteCity").prop("disabled",true);
		}else{
			$("#instituteCity").prop("disabled",false);
			 
			var state = $("#instituteState").val();
			$.ajax({
					type : "GET",
					contentType : "application/json;charset=utf-8",
					data : {'state': state},
					url : "cities",
					dataType : 'json',
					success : function(response) {
						$("#instituteCity").empty();
						$("<option/>").attr("value","").text("Select City").appendTo($("#instituteCity"));
						for(var i=0;i<response.length;i++){
							$("<option/>").attr("value",response[i].cityId).text(response[i].cityName).appendTo($("#instituteCity"));
						}
					},
					error : function(e) {
						$("#instituteCity").empty();
						$("<option/>").attr("value","").text("Select City").appendTo($("#instituteCity"));
						//alert('error');
					}
				}); 
		}
		 
	}
}

function populateIndianCities(){
	if($('#schoolState').val() == ""){
		$("#schoolCity").prop("disabled",true);
	}else{
		$("#schoolCity").prop("disabled",false);
		var state = $("#schoolState").val();
		$.ajax({
			type : "GET",
			contentType : "application/json;charset=utf-8",
			data : {'state': state},
			url : "cities",
			dataType : 'json',
			success : function(response) {
				$("#schoolCity").empty();
				$("<option/>").attr("value","").text("Select City").appendTo($("#schoolCity"));
				for(var i=0;i<response.length;i++){
					$("<option/>").attr("value",response[i].cityName).text(response[i].cityName).appendTo($("#schoolCity"));
				}
			},
			error : function(e) {
				$("#schoolCity").empty();
				$("<option/>").attr("value","").text("Select City").appendTo($("#schoolCity"));
				//alert('error');
			}
		}); 
		 
	}
}



