function validateLoginPage(){
	$('#signinform').bootstrapValidator({
		container: 'tooltip',
	    feedbackIcons: {
	    	valid: 'glyphicon glyphicon-ok',
	    	invalid: 'glyphicon glyphicon-remove',
	    	validating: 'glyphicon glyphicon-refresh'
	    },
	    fields: {
	    	username: {
	    		message: 'The username is not valid',
	    		validators: {
	    			notEmpty: {
	    				message: 'The username is required and cannot be empty'
	    			},
	    			stringLength: {
	    				min: 6,
	    				max: 30,
	    				message: 'The username must be more than 6 and less than 30 characters long'
	    			},
                    regexp: {
                        regexp: /^[a-zA-Z0-9]+$/,
                        message: 'The username can only consist of alphabetical and number'
                    },
                    different: {
                        field: 'password',
                        message: 'The username and password cannot be the same as each other'
                    }
	    		}
	    	},
            password: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
                    different: {
                        field: 'username',
                        message: 'The password cannot be the same as username'
                    },
                    stringLength: {
                        min: 8,
                        message: 'The password must have at least 8 characters'
                    }
                }
            }
	    }
	});
}


function validateRegistrationPage(){
	$('#registrationform').bootstrapValidator({
		container: 'tooltip',
	    feedbackIcons: {
	    	valid: 'glyphicon glyphicon-ok',
	    	invalid: 'glyphicon glyphicon-remove',
	    	validating: 'glyphicon glyphicon-refresh'
	    },
        fields: {
        	username: {
        		message: 'The username is not valid',
                validators: {
                	notEmpty: {
                		message: 'The username is required and cannot be empty'
                	},
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'The username must be more than 6 and less than 30 characters long'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9]+$/,
                        message: 'The username can only consist of alphabetical and number'
                    },
                    different: {
                        field: 'password',
                        message: 'The username and password cannot be the same as each other'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
                    different: {
                        field: 'username',
                        message: 'The password cannot be the same as username'
                    },
                    stringLength: {
                        min: 8,
                        message: 'The password must have at least 8 characters'
                    }
                }
            },
            'student.email': {
            	validators: {  
                    notEmpty: {  
                        message: 'The email is required and can\'t be empty'  
                    },
                    emailAddress: {
                        message: 'The email is not valid.'
                    }
                } 
            },
            'student.firstName': {
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    }
                }
            },
            'student.lastName': {
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    }
                }
            },
            'student.contactNo': {
                validators: {
                    notEmpty: {
                        message: 'The contact no is required and cannot be empty'
                    }
                }
            },
            'student.studentAddress.city': {
                validators: {
                    notEmpty: {
                        message: 'The city is required and cannot be empty'
                    }
                }
            },
            'student.studentAddress.state': {
                validators: {
                    notEmpty: {
                        message: 'The state is required and cannot be empty'
                    }
                }
            },
            'student.studentAddress.country': {
                validators: {
                    notEmpty: {
                        message: 'The country is required and cannot be empty'
                    }
                }
            },
            'student.studentAddress.zipCode': {
                validators: {
                    notEmpty: {
                        message: 'The zip is required and cannot be empty'
                    }
                }
            },
            'institute.email': {
            	validators: {  
                    notEmpty: {  
                        message: 'The email is required and can\'t be empty'  
                    },
                    emailAddress: {
                        message: 'The email is not valid.'
                    }
                } 
            },
            'institute.contactNo': {
                validators: {
                    notEmpty: {
                        message: 'The contact no is required and cannot be empty'
                    }
                }
            },
            'institute.instituteName': {
                validators: {
                    notEmpty: {
                        message: 'The name is required and cannot be empty'
                    }
                }
            },
            'institute.instituteAddress.city': {
                validators: {
                    notEmpty: {
                        message: 'The city is required and cannot be empty'
                    }
                }
            },
            'institute.instituteAddress.state': {
                validators: {
                    notEmpty: {
                        message: 'The state is required and cannot be empty'
                    }
                }
            },
            'institute.instituteAddress.country': {
                validators: {
                    notEmpty: {
                        message: 'The country is required and cannot be empty'
                    }
                }
            },
            'institute.instituteAddress.zipCode': {
                validators: {
                    notEmpty: {
                        message: 'The zip is required and cannot be empty'
                    }
                }
            }
        }
    });
}

function validateAfterLoginDetailsPage(){
	
	$('#after-login-form').bootstrapValidator({
		container: 'tooltip',
	    feedbackIcons: {
	    	valid: 'glyphicon glyphicon-ok',
	    	invalid: 'glyphicon glyphicon-remove',
	    	validating: 'glyphicon glyphicon-refresh'
	    },
        fields: {
            'student.school': {
                validators: {
                    notEmpty: {
                        message: 'The school name is required and cannot be empty'
                    }
                }
            },
            'student.schoolCity': {
                validators: {
                    notEmpty: {
                        message: 'The city is required and cannot be empty'
                    }
                }
            },
            'student.schoolState': {
                validators: {
                    notEmpty: {
                        message: 'The state is required and cannot be empty'
                    }
                }
            },
            'student.studentClass': {
                validators: {
                    notEmpty: {
                        message: 'The student class is required and cannot be empty'
                    },
                    callback: {
                        message: 'The student class is required',
                        callback: function(value, validator, $field) {
                            if (value === '0') {
                                return false;
                            }
                            
                            if (value !== '0') {
                                return true;
                            }
                        }
                    }
                }
            },
            'student.board.boardId': {
                validators: {
                    notEmpty: {
                        message: 'The board name is required and cannot be empty'
                    },
                    callback: {
                        message: 'The board name is required',
                        callback: function(value, validator, $field) {
                            if (value === '0') {
                                return false;
                            }
                            if (value !== '0') {
                                return true;
                            }
                        }
                    }
            }
        }
        }
    });
}


function UpdateContactDetails(){
	$('#profileUpdateform').bootstrapValidator({
		container: 'tooltip',
	    feedbackIcons: {
	    	valid: 'glyphicon glyphicon-ok',
	    	invalid: 'glyphicon glyphicon-remove',
	    	validating: 'glyphicon glyphicon-refresh'
	    },
        fields: {
        	'student.contactNo': {  
                validators: {  
                    notEmpty: {  
                        message: 'The contactNo is required and can\'t be empty'  
                    }
                }  
            },
            'student.email': {  
                validators: {  
                	notEmpty: {  
                        message: 'The email is required and can\'t be empty'  
                    },
                    emailAddress: {
                        message: 'The email is not valid.'
                    }
                }  
            }
        }
    });
}

function updateAccountDetails(){
	$('#updateAccountDetailsForm').bootstrapValidator({
		container: 'tooltip',
	    feedbackIcons: {
	    	valid: 'glyphicon glyphicon-ok',
	    	invalid: 'glyphicon glyphicon-remove',
	    	validating: 'glyphicon glyphicon-refresh'
	    },
        fields: {
        	curPassword: {  
                validators: {  
                    notEmpty: {  
                        message: 'The current password is required and can\'t be empty'  
                    }
                }  
            },
            newPassword: {  
                validators: {  
                    notEmpty: {  
                        message: 'The password is required and can\'t be empty'  
                    },  
                    identical: {  
                        field: 'reNewPassword',  
                        message: 'The password and its confirm are not the same'  
                    } 
                }  
            },  
            reNewPassword: {  
                validators: {  
                    notEmpty: {  
                        message: 'The confirm password is required and can\'t be empty'  
                    },  
                    identical: {  
                        field: 'newPassword',  
                        message: 'The password and its confirm are not the same'  
                    } 
                }  
            }
        }
    });
}

function updateEducationalDetails(){
	$('#updateEducationalDetailsForm').bootstrapValidator({
		container: 'tooltip',
	    feedbackIcons: {
	    	valid: 'glyphicon glyphicon-ok',
	    	invalid: 'glyphicon glyphicon-remove',
	    	validating: 'glyphicon glyphicon-refresh'
	    },
        fields: {
            'student.school': {
                validators: {
                    notEmpty: {
                        message: 'The school name is required and cannot be empty'
                    }
                }
            },
            'student.schoolCity': {
                validators: {
                    notEmpty: {
                        message: 'The city is required and cannot be empty'
                    }
                }
            },
            'student.schoolState': {
                validators: {
                    notEmpty: {
                        message: 'The state is required and cannot be empty'
                    }
                }
            },
            'student.studentClass': {
                validators: {
                    notEmpty: {
                        message: 'The student class is required and cannot be empty'
                    },
                    callback: {
                        message: 'The student class is required',
                        callback: function(value, validator, $field) {
                            if (value === '0') {
                                return false;
                            }if (value !== '0') {
                                return true;
                            }
                        }
                    }
                }
            },
            'student.board.boardId': {
                validators: {
                    notEmpty: {
                        message: 'The board name is required and cannot be empty'
                    },
                    callback: {
                        message: 'The board name is required',
                        callback: function(value, validator, $field) {
                            if (value === '0') {
                                return false;
                            }if (value !== '0') {
                                return true;
                            }
                        }
                    }
            	}
        	}
        }
    });
}

function resetPasswordValidation(){
	$(document).ready(function() {
	    $('#reset-password-form').bootstrapValidator({
	    	container: 'tooltip',
	        icon: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },
	        fields: {
	        	accessCode: {  
	                validators: {  
	                    notEmpty: {  
	                        message: 'The accessCode is required and can\'t be empty'  
	                    }
	                }  
	            },
	            newPassword: {  
	                validators: {  
	                    notEmpty: {  
	                        message: 'The password is required and can\'t be empty'  
	                    },  
	                    identical: {  
	                        field: 'reNewPassword',  
	                        message: 'The password and its confirm are not the same'  
	                    } 
	                }  
	            },  
	            reNewPassword: {  
	                validators: {  
	                    notEmpty: {  
	                        message: 'The confirm password is required and can\'t be empty'  
	                    },  
	                    identical: {  
	                        field: 'newPassword',  
	                        message: 'The password and its confirm are not the same'  
	                    } 
	                }  
	            }
	        }
	    });
	});
}
