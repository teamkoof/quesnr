/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Raviraj
 *
 */
package com.quesnr.user.management.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.quesnr.constants.QuesnrConstants;
import com.quesnr.exception.QuesnrException;
import com.quesnr.generic.service.QuesnrGenericService;
import com.quesnr.registration.management.controller.RegistrationController;
import com.quesnr.student.management.service.StudentService;
import com.quesnr.user.management.model.Institute;
import com.quesnr.user.management.model.Student;
import com.quesnr.user.management.model.User;
import com.quesnr.user.management.model.UserRole;
import com.quesnr.user.management.service.UserService;
import com.quesnr.util.HashCode;
import com.quesnr.util.QuesnrUtil;
import com.quesnr.util.email.service.EmailService;

@SessionAttributes("sessionUser")
@Controller
public class ManageUserController {

	private static final Logger logger = LogManager.getLogger(RegistrationController.class);
	
	@Autowired
	UserService userService;
	
	@Autowired
	StudentService studentService;
	
	@Autowired
	EmailService emailService;
	
	@Autowired
	QuesnrUtil quesnrUtil;
	@Autowired
	HashCode hashCode;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	QuesnrGenericService quesnrGenericService;
	
	@RequestMapping(value = { "/home" }, method = RequestMethod.GET)
	public String home(ModelMap model,@ModelAttribute("user") User user,@ModelAttribute("displayUser") User displayUser, @ModelAttribute("student")Student student, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			HttpSession session) {
		
		user = (User) session.getAttribute("userDetails");
		getPrincipal();
		UserRole userRole = (UserRole) session.getAttribute("userRole");
		
		logger.info("Logged-in with username as '"+user.getUsername()+"' and role as '"+user.getRole()+"'. IPAddress : "+user.getIpAddress());
		
		if(userRole.getRole().equals(QuesnrConstants.ROLE_STUDENT)){
			displayUser.setName(user.getStudent().getFirstName().concat(" ").concat(user.getStudent().getLastName()));
			displayUser.setEmail(user.getStudent().getEmail());
			displayUser.setContactNo(user.getStudent().getContactNo());
			model.addAttribute("role", QuesnrConstants.STUDENT);
		}else if(userRole.getRole().equals(QuesnrConstants.ROLE_INSTITUTE)){
			displayUser.setName(user.getInstitute().getInstituteName());
			displayUser.setEmail(user.getInstitute().getEmail());
			displayUser.setContactNo(user.getInstitute().getContactNo());
			model.addAttribute("role", QuesnrConstants.INSTITUTE);
		}
		model.addAttribute("stateList",session.getAttribute("stateList"));
 		model.addAttribute("cityList",session.getAttribute("cityList"));
		model.addAttribute("user", user);
		model.addAttribute("displayUser", displayUser);
		
		if(user != null && !user.isBasicDetails()){
			model.addAttribute("boardList", session.getAttribute("boardList"));
			if(user.getRole() != null && user.getRole().equals(QuesnrConstants.ROLE_STUDENT)){
	 			model.addAttribute("role",QuesnrConstants.STUDENT);
	 		}else if(user.getRole() != null && user.getRole().equals(QuesnrConstants.ROLE_INSTITUTE)){
	 			model.addAttribute("role",QuesnrConstants.INSTITUTE);
	 		}
			return "welcomePages/after-login-details";
		}else{
			logger.info("Redirecting to home page");			
			return "home";
		}		
	}
	
	@RequestMapping(value={"/profile"}, method = RequestMethod.GET)
	public String viewProfile(ModelMap model,@ModelAttribute("user") User user,@ModelAttribute("displayUser") User displayUser, 
			@ModelAttribute("student")Student student, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			HttpSession session) {
		logger.info("Redirecting to profile page");
		user = (User) session.getAttribute("userDetails");
		getPrincipal();
		UserRole userRole = (UserRole) session.getAttribute("userRole");
 		
		if(userRole.getRole().equals(QuesnrConstants.ROLE_STUDENT)){
			displayUser.setName(user.getStudent().getFirstName().concat(" ").concat(user.getStudent().getLastName()));
			displayUser.setEmail(user.getStudent().getEmail());
			displayUser.setContactNo(user.getStudent().getContactNo());
			model.addAttribute("role", QuesnrConstants.STUDENT);
		}else if(userRole.getRole().equals(QuesnrConstants.ROLE_INSTITUTE)){
			displayUser.setName(user.getInstitute().getInstituteName());
			displayUser.setEmail(user.getInstitute().getEmail());
			displayUser.setContactNo(user.getInstitute().getContactNo());
			model.addAttribute("role", QuesnrConstants.INSTITUTE);
		}
		model.addAttribute("stateList",session.getAttribute("stateList"));
 		model.addAttribute("cityList",session.getAttribute("cityList"));
		model.addAttribute("user", user);
		model.addAttribute("displayUser", displayUser);
		
	    return "profile";
				
	}
	
	private String getPrincipal(){
 		String userName = null;
		Object principal =
		SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal instanceof UserDetails) {
			userName = ((UserDetails)principal).getUsername();
		} else {
			userName = principal.toString();
		}
		return userName;
 	}
	
	
	@RequestMapping(value={"/changeProfilePic"}, method = RequestMethod.POST)
	public @ResponseBody HashMap<String, Object> changeProfilePic(MultipartHttpServletRequest request, HttpServletResponse httpServletResponse) throws IOException{
		logger.info("Changing the Profile Picture");
		/*user = (User) session.getAttribute("userDetails");
		getPrincipal();
		
		logger.info("data : "+user.getProfilePic());*/
		
		    MultipartFile multipartFile = request.getFile("profilePic");
	        Long size = multipartFile.getSize();
	        String contentType = multipartFile.getContentType();
	        InputStream stream = multipartFile.getInputStream();
	        byte[] bytes = IOUtils.toByteArray(stream);
	    
	        HashMap<String, Object> map = new HashMap<String, Object>();
	        map.put("fileoriginalsize", size);
	        map.put("contenttype", contentType);
	        map.put("base64", new String(Base64Utils.encode(bytes)));
	    
	        return map;
	}
	
	@RequestMapping(value="/update-profile", method = RequestMethod.POST)
	public String updateProfile(ModelMap model, @ModelAttribute("user") User user, @ModelAttribute("displayUser") User displayUser, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			HttpSession session) {
		
		boolean isUpdated = false;
		User originalUser = (User) session.getAttribute("userDetails");
		user = user.copyUser(user, originalUser);
		logger.info("Updating Profile for user : '"+user.getUsername()+"'");
		
		if(user!=null){
			isUpdated = this.userService.updateUser(user);
		}
		
		if(isUpdated){
			model.addAttribute("updateMsg", "Your account is updated successfully !");
		}else{
			model.addAttribute("errorUpdateMsg", "Your account could not be updated !");
		}
        UserRole userRole = (UserRole) session.getAttribute("userRole");
		
		if(userRole.getRole().equals(QuesnrConstants.ROLE_STUDENT)){
			displayUser.setName(user.getStudent().getFirstName().concat(" ").concat(user.getStudent().getLastName()));
			displayUser.setEmail(user.getStudent().getEmail());
			displayUser.setContactNo(user.getStudent().getContactNo());
			model.addAttribute("role", QuesnrConstants.STUDENT);
		}else if(userRole.getRole().equals(QuesnrConstants.ROLE_INSTITUTE)){
			displayUser.setName(user.getInstitute().getInstituteName());
			displayUser.setEmail(user.getInstitute().getEmail());
			displayUser.setContactNo(user.getInstitute().getContactNo());
			model.addAttribute("role", QuesnrConstants.INSTITUTE);
		}
		model.addAttribute("user", user);
		model.addAttribute("displayUser", displayUser);
		
	    return "profile";
				
	}
	
	@RequestMapping(value="/forgot-password", method = RequestMethod.GET)
	public String forgotPassword(ModelMap model, @ModelAttribute("user") User user, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			HttpSession session) {
		logger.info("In forgot password method..");
		model.addAttribute("user", user);
		return "welcomePages/forgot-password";
	}
	
	@RequestMapping(value="/forgot-password-send-email", method = RequestMethod.POST)
	public String SendVerificationLinkToResetPassword(ModelMap model, @ModelAttribute("user") User user, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			HttpSession session) {
		logger.info("In forgot password method..");
		String email = user.getEmail();
		Object obj = userService.checkUserRoleByEmail(email);
		String accessCode = quesnrUtil.generateAccessCode();
		if(obj!=null){
			if(obj instanceof Student){
				Student s = (Student) obj;
				
				if(email!=null && !email.equals("")){
					try {
						emailService.sendForgotPasswordEmailNotification(user,s.getStudentId(),accessCode,QuesnrConstants.STUDENT);
					} catch (QuesnrException e) {
						// TODO Auto-generated catch block
						logger.info("",e.getErrorCode()+ " " +e.getMessage());
						model.addAttribute("errorCode",e.getErrorCode());
						model.addAttribute("An error occured while processing your request.", e.getMessage());
						return "welcomePages/error-page";
					}
					userService.updateAccessCode(accessCode, s.getStudentId(), QuesnrConstants.STUDENT);
				}
			}else if(obj instanceof Institute){
				Institute i = (Institute) obj;
				if(email!=null && !email.equals("")){
					try {
						emailService.sendForgotPasswordEmailNotification(user,i.getInstituteId(),accessCode,QuesnrConstants.INSTITUTE);
					} catch (QuesnrException e) {
						// TODO Auto-generated catch block
						logger.info("",e.getErrorCode()+ " " +e.getMessage());
						model.addAttribute("errorCode",e.getErrorCode());
						model.addAttribute("An error occured while processing your request.", e.getMessage());
						return "welcomePages/error-page";
					}
					userService.updateAccessCode(accessCode, i.getInstituteId(), QuesnrConstants.INSTITUTE);
					
				}
			}
		}
		
		model.addAttribute("user", user);
		model.addAttribute("message",QuesnrConstants.RESET_PASSWORD);
		return "welcomePages/entry";
	}
	
	@RequestMapping(value="/resetPassword", method = RequestMethod.GET)
	public String resetPassword(ModelMap model,@ModelAttribute("user") User user,@RequestParam(value="email")String email,@RequestParam(value="accessCode")String accessCode,
			@RequestParam(value="id")String id,HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,@RequestParam(value="role")String role,
			HttpSession session) {
		logger.info("In resetPassword method..");
		logger.info("email : "+email);
		logger.info("accessCode : "+accessCode);
		logger.info("id : " +id);
		
		model.addAttribute("user", user);
		model.addAttribute("email", email);
		model.addAttribute("accessCode", accessCode);
		model.addAttribute("id", id);
		
		return "welcomePages/reset-password";
	}
	
	@RequestMapping(value="/update-password", method = RequestMethod.POST)
	public String updatePassword(ModelMap model,@ModelAttribute("user") User user,@RequestParam(value="email")String email, 
			
			HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			HttpSession session) throws QuesnrException {
		logger.info("In update Password method..");
		
		boolean accessCode = userService.checkAccessCode(user.getAccessCode(),user.getId(),user.getRole());
		String password = passwordEncoder.encode(user.getNewPassword());
		if(accessCode){
			if(user.getRole().equals(QuesnrConstants.STUDENT)){
				userService.updatePassword(password, user.getId(), QuesnrConstants.STUDENT);
			}else{
				userService.updatePassword(password, user.getId(), QuesnrConstants.INSTITUTE);
			}
		}else{
			throw new QuesnrException("An error occured while processing your request.", "Access code is not valid.");
		}
		
		return "welcomePages/entry";
	}
	
	@ExceptionHandler({Exception.class,QuesnrException.class })
    public ModelAndView handleIOException(Exception ex) {
        ModelAndView model = new ModelAndView("welcomePages/error-page");
        model.addObject("exception", ex.getMessage());
        return model;
    }
	
	
	@RequestMapping(value="/update-Profile-password", method = RequestMethod.POST)
	public String updateProfilePassword(ModelMap model,@ModelAttribute("user") User user,
			HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			HttpSession session) throws QuesnrException {
		logger.info("In update Password method..");
		User originalUser = (User) session.getAttribute("userDetails");
		boolean checkPassword = passwordEncoder.matches(user.getCurPassword(), originalUser.getPassword());
		
		if(checkPassword){
			String newPassword = passwordEncoder.encode(user.getNewPassword());
			userService.updateProfilePassword(user.getUserId(), newPassword);
			model.addAttribute("updateMsg", "Password updated successfully.");
		}else{
			model.addAttribute("errorUpdateMsg", "Your current password does not matches with the password you provided.Please try again!");
		}
		
		return "profile";
	}

}
