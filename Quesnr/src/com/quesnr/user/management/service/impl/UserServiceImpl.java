/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Ruchira
 *
 */

package com.quesnr.user.management.service.impl;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.quesnr.user.management.dao.UserDAO;
import com.quesnr.user.management.model.User;
import com.quesnr.user.management.model.UserRole;
import com.quesnr.user.management.service.UserService;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{
	
	private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);
	
	@Autowired
	private UserDAO userDAO;

	@Override
	public void createUser(User user) {
		// TODO Auto-generated method stub
		logger.info("In create user service method");
		userDAO.createUser(user);
	}

	@Override
	public User findUserByUserId(long userId) {
		// TODO Auto-generated method stub
		return userDAO.findUserByUserId(userId);
	}

	@Override
	public User findUserByUserName(String username) {
		// TODO Auto-generated method stub
		return userDAO.findUserByUserName(username);
	}

	@Override
	public boolean updateUser(User user) {
		// TODO Auto-generated method stub
		 boolean isUpdated = false; 
		 isUpdated =  userDAO.updateUser(user);
		 return isUpdated;
	}

	@Override
	public UserRole findUserRoleByUserId(long userId) {
		// TODO Auto-generated method stub
		return userDAO.findUserRoleByUserId(userId);
	}

	@Override
	public List<User> getUserList(String userRole) {
		// TODO Auto-generated method stub
		return userDAO.getUserList(userRole);
	}

	@Override
	public List<String> getCountryList() {
		// TODO Auto-generated method stub
		return userDAO.getCountryList();
	}

	@Override
	public Object checkUserRoleByEmail(String email) {
		// TODO Auto-generated method stub
		return userDAO.checkUserRoleByEmail(email);
	}

	@Override
	public void updateAccessCode(String accessCode, long id,String role) {
		// TODO Auto-generated method stub
		userDAO.updateAccessCode(accessCode, id, role);
	}

	@Override
	public boolean checkAccessCode(String accessCode,long id,String role) {
		// TODO Auto-generated method stub
		return userDAO.checkAccessCode(accessCode,id,role);
	}

	@Override
	public void updatePassword(String password,long id,String role) {
		// TODO Auto-generated method stub
		userDAO.updatePassword(password, id,role);
	}

	@Override
	public void updateProfilePassword(long userId, String password) {
		// TODO Auto-generated method stub
		userDAO.updateProfilePassword(userId, password);
	}
	
}
