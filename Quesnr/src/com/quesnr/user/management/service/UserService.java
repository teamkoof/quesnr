/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Ruchira
 *
 */

package com.quesnr.user.management.service;
import java.util.List;

import com.quesnr.user.management.model.User;
import com.quesnr.user.management.model.UserRole;

public interface UserService {
	public void createUser(User user);
	public User findUserByUserId(long userId);
	public User findUserByUserName(String username);
	public boolean updateUser(User user);
	public UserRole findUserRoleByUserId(long userId);
	public List<User> getUserList(String userRole);
	public List<String> getCountryList();
	public Object checkUserRoleByEmail(String email);
	public void updateAccessCode(String accessCode, long id,String role);
	public boolean checkAccessCode(String accessCode,long id,String role);
	public void updatePassword(String password,long id,String role);
	public void updateProfilePassword(long userId,String password);
	
	
}
