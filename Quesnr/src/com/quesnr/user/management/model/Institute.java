/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Ruchira
 *
 */
package com.quesnr.user.management.model;
public class Institute {
	private long instituteId;
	private String instituteName;
	private String email;
	private String contactNo;
	private Address instituteAddress;
	/**
	 * @return the instituteId
	 */
	public long getInstituteId() {
		return instituteId;
	}
	/**
	 * @param instituteId the instituteId to set
	 */
	public void setInstituteId(long instituteId) {
		this.instituteId = instituteId;
	}
	/**
	 * @return the instituteName
	 */
	public String getInstituteName() {
		return instituteName;
	}
	/**
	 * @param instituteName the instituteName to set
	 */
	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the contactNo
	 */
	public String getContactNo() {
		return contactNo;
	}
	/**
	 * @param contactNo the contactNo to set
	 */
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	/**
	 * @return the instituteAddress
	 */
	public Address getInstituteAddress() {
		return instituteAddress;
	}
	/**
	 * @param instituteAddress the instituteAddress to set
	 */
	public void setInstituteAddress(Address instituteAddress) {
		this.instituteAddress = instituteAddress;
	}
	
}
