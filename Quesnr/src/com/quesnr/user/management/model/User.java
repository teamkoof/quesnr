/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Ruchira
 *
 */
package com.quesnr.user.management.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.quesnr.constants.QuesnrConstants;

public class User {
	private long userId;
	private String username;
	private String password;
	private String ipAddress;
	private String jSessionId;
	private String createdBy;
	private Date dateCreated;
	private String updatedBy;
	private Date updatedDate;
	private Boolean enabled;
	private Date lastLoggedInTime;
	private String role;
	private Set<UserRole> userRole = new HashSet<UserRole>(0);
	private Boolean verified;
	private long loginCount;
	private Boolean agreeTerms;
	private Student student;
	private Institute institute;
	private Boolean basicDetails;
	private byte[] profilePic;
	
	//for change password
	private String curPassword;
	private String newPassword;
	private String reNewPassword;
	
	private String accessCode;
	//web display only
	private String name;
	private String email;
	private String contactNo;
	private long id;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public User() {
		super();
	}

 	public User(String username, String password, boolean enabled) {
		 this.username = username;
		 this.password = password;
		 this.enabled = enabled;
	}
	
 	public User(String username, String password, boolean enabled, Set<UserRole> userRole) {
		 this.username = username;
		 this.password = password;
		 this.enabled = enabled;
		 this.userRole = userRole;
	}
 	

	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param ipAddress the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @return the jSessionId
	 */
	public String getjSessionId() {
		return jSessionId;
	}

	/**
	 * @param jSessionId the jSessionId to set
	 */
	public void setjSessionId(String jSessionId) {
		this.jSessionId = jSessionId;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the dateCreated
	 */
	public Date getDateCreated() {
		return dateCreated;
	}

	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	/**
	 * @return the updatedBy
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return the updatedDate
	 */
	public Date getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * @param updatedDate the updatedDate to set
	 */
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	/**
	 * @return the enabled
	 */
	public Boolean getEnabled() {
		return enabled;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}
	
	/**
	 * @return the lastLoggedInTime
	 */
	public Date getLastLoggedInTime() {
		return lastLoggedInTime;
	}

	/**
	 * @param lastLoggedInTime the lastLoggedInTime to set
	 */
	public void setLastLoggedInTime(Date lastLoggedInTime) {
		this.lastLoggedInTime = lastLoggedInTime;
	}

	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * @return the userRole
	 */
	public Set<UserRole> getUserRole() {
		return userRole;
	}

	/**
	 * @param userRole the userRole to set
	 */
	public void setUserRole(Set<UserRole> userRole) {
		this.userRole = userRole;
	}

	/**
	 * @return the verified
	 */
	public Boolean getVerified() {
		return verified;
	}

	/**
	 * @param verified the verified to set
	 */
	public void setVerified(Boolean verified) {
		this.verified = verified;
	}

	public boolean isVerified() {
		return verified;
	}
	/**
	 * @return the loginCount
	 */
	public long getLoginCount() {
		return loginCount;
	}

	/**
	 * @param loginCount the loginCount to set
	 */
	public void setLoginCount(long loginCount) {
		this.loginCount = loginCount;
	}

	public Boolean getAgreeTerms() {
		return agreeTerms;
	}

	public void setAgreeTerms(Boolean agreeTerms) {
		this.agreeTerms = agreeTerms;
	}
	
	public Boolean isAgreeTerms() {
		return agreeTerms;
	}

	/**
	 * @return the student
	 */
	public Student getStudent() {
		return student;
	}

	/**
	 * @param student the student to set
	 */
	public void setStudent(Student student) {
		this.student = student;
	}

	/**
	 * @return the institute
	 */
	public Institute getInstitute() {
		return institute;
	}

	/**
	 * @param institute the institute to set
	 */
	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	public Boolean getBasicDetails() {
		return basicDetails;
	}

	public void setBasicDetails(Boolean basicDetails) {
		this.basicDetails = basicDetails;
	}

	public Boolean isBasicDetails() {
		return basicDetails;
	}

	public byte[] getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(byte[] profilePic) {
		this.profilePic = profilePic;
	}

	public String getCurPassword() {
		return curPassword;
	}

	public void setCurPassword(String curPassword) {
		this.curPassword = curPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getReNewPassword() {
		return reNewPassword;
	}

	public void setReNewPassword(String reNewPassword) {
		this.reNewPassword = reNewPassword;
	}


	public User copyUser(User user, User originalUser) {
		//User sessionUser = (User) originalUser.getAttribute("userDetails");
		Date updatedDate = new Date(); 
		if(originalUser != null && originalUser.getUserId()== user.getUserId()){
			
			if(user.getUsername() != null){
				originalUser.setUsername(user.getUsername());
			}
			if(user.getPassword() != null){
				originalUser.setPassword(user.getPassword());
			}
			
			if(originalUser.getRole().equals(QuesnrConstants.ROLE_STUDENT)){				
				if(user.getStudent().getEmail() != null){
					originalUser.getStudent().setEmail(user.getStudent().getEmail());
				}
				if(user.getStudent().getContactNo() != null){
					originalUser.getStudent().setContactNo(user.getStudent().getContactNo());
				}				
				if(user.getStudent().getStudentClass() != 0){
					originalUser.getStudent().setStudentClass(user.getStudent().getStudentClass());
				}
				if(user.getStudent().getSchool() != null){
					originalUser.getStudent().setSchool(user.getStudent().getSchool());
				}
				if(user.getStudent().getSchoolCity() != null){
					originalUser.getStudent().setSchoolCity(user.getStudent().getSchoolCity());
				}
				if(user.getStudent().getSchoolState() != null){
					originalUser.getStudent().setSchoolState(user.getStudent().getSchoolState());
				}
				if(user.getStudent().getBoard() != null){
					if(String.valueOf(user.getStudent().getBoard().getBoardId()) != null){
						originalUser.getStudent().getBoard().setBoardId(user.getStudent().getBoard().getBoardId());
					}				
				}
				
			}else if(originalUser.getRole().equals(QuesnrConstants.ROLE_INSTITUTE)){
				if(user.getInstitute().getInstituteName() == null){
					user.getInstitute().setInstituteName(originalUser.getInstitute().getInstituteName());
				}
				if(user.getInstitute().getEmail() == null){
					user.getInstitute().setEmail(originalUser.getInstitute().getEmail());
				}
				if(user.getInstitute().getContactNo() == null){
					user.getInstitute().setContactNo(originalUser.getInstitute().getContactNo());
				}
				if(user.getInstitute().getInstituteAddress()==null){
					user.getInstitute().setInstituteAddress(originalUser.getInstitute().getInstituteAddress());
				}
			}	
		}else{
			return null;
		}
		
		return originalUser;
	}

	/**
	 * @return the accessCode
	 */
	public String getAccessCode() {
		return accessCode;
	}

	/**
	 * @param accessCode the accessCode to set
	 */
	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	
	
	
}
