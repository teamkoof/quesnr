/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Ruchira
 *
 */
package com.quesnr.user.management.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.quesnr.constants.QuesnrConstants;
import com.quesnr.generic.model.Country;
import com.quesnr.user.management.dao.UserDAO;
import com.quesnr.user.management.model.Institute;
import com.quesnr.user.management.model.Student;
import com.quesnr.user.management.model.User;
import com.quesnr.user.management.model.UserRole;

@Repository("userDAO")
@Transactional
public class UserDAOImpl implements UserDAO{
	
	private static final Logger logger = LogManager.getLogger(UserDAOImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void createUser(User user) {
		// TODO Auto-generated method stub
		logger.info("In create user dao method");
		this.sessionFactory.getCurrentSession().saveOrUpdate(user);	
	}

	@SuppressWarnings("unchecked")
	@Override
	public User findUserByUserId(long userId) {
		// TODO Auto-generated method stub
		List<User> userList = new ArrayList<User>();
		userList = this.sessionFactory.getCurrentSession().createQuery("from User where userId=?").setParameter(0, userId).list();

		if (userList.size() > 0)
			return userList.get(0);
		else
		 return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public User findUserByUserName(String username) {
		// TODO Auto-generated method stub
		List<User> userList = new ArrayList<User>();
		userList = this.sessionFactory.getCurrentSession().createQuery("from User where username=?").setParameter(0, username).list();

		if (userList.size() > 0)
			return userList.get(0);
		else
		 return null;
	}

	@Override
	public boolean updateUser(User user) {
		// TODO Auto-generated method stub
		boolean isUpdated = false; 
		try{
			this.sessionFactory.getCurrentSession().update(user);
			isUpdated = true;
		}catch(Exception e){
			logger.info("You have an exception! User "+user.getUserId()+" cannot be updated.", e);					
		}	
		return isUpdated;
	}

	@SuppressWarnings("unchecked")
	@Override
	public UserRole findUserRoleByUserId(long userId) {
		// TODO Auto-generated method stub
		List<UserRole> userRoleList = new ArrayList<UserRole>();
		userRoleList = this.sessionFactory.getCurrentSession().createQuery("from UserRole where user_id=?").setParameter(0, userId).list();

		if (userRoleList.size() > 0)
			return userRoleList.get(0);
		else
		 return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getUserList(String userRole) {
		// TODO Auto-generated method stub
		List<User> userList = new ArrayList<User>();
		userList = this.sessionFactory.getCurrentSession().createQuery("from User where role=?").setParameter(0, userRole).list();

		if (userList.size() > 0)
			return userList;
		else
		 return null;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<String> getCountryList() {
		// TODO Auto-generated method stub
		List<Country> countryList = new ArrayList<Country>();
		countryList = this.sessionFactory.getCurrentSession().createQuery("from Country").list();
		Iterator it = countryList.iterator();
		List<String> countryList1 = new ArrayList<String>();
		while(it.hasNext())
		 {
			Object o = (Object)it.next();
			Country c = (Country)o;
		 //System.out.println("Product id : "+c.getCountryName());
		 countryList1.add(c.getCountryName());
		 } 
		if (countryList.size() > 0)
			return countryList1;
		else
		 return null;
	}
	
	public void updatePassword(String password,String email){
	/* Query q = this.sessionFactory.getCurrentSession().createQuery("from User where password = :password ");
	   q.setParameter("password", password);
	   User user = (User)q.list().get(0);

	   user.setPassword(password);
	   session.update(stockTran);*/
	}
	


	@SuppressWarnings("unchecked")
	@Override
	public Object checkUserRoleByEmail(String email) {
		// TODO Auto-generated method stub
		List<Student> studentList = new ArrayList<Student>();
		studentList = this.sessionFactory.getCurrentSession().createQuery("from Student where email=?").setParameter(0, email).list();
		
		if (studentList.size() > 0){
			return studentList.get(0);
		}else{
			List<Institute> instititeList = new ArrayList<Institute>();
			instititeList = this.sessionFactory.getCurrentSession().createQuery("from Institute where email=?").setParameter(0, email).list();
			return instititeList.get(0);
		}
		
		
		
	}

	@Override
	public void updateAccessCode(String accessCode, long id,String role) {
		// TODO Auto-generated method stub
		if(role.equals(QuesnrConstants.STUDENT)){
			String hql = "update user u set u.access_code=? " + " where u.student_id=?";
			Query query = this.sessionFactory.getCurrentSession().createSQLQuery(hql);
			query.setString(0, accessCode);
			query.setLong(1, id);
			query.executeUpdate();
			
		}else{
			String hql = "update user u set u.access_code=? " + " where u.institute_id=?";
			Query query = this.sessionFactory.getCurrentSession().createSQLQuery(hql);
			query.setString(0, accessCode);
			query.setLong(1, id);
			query.executeUpdate();
		}
		
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean checkAccessCode(String accessCode,long id,String role) {
		// TODO Auto-generated method stub
		/*List<User> userList = new ArrayList<User>();
		String query = "from User where accessCode=? and student_i";
		userList = this.sessionFactory.getCurrentSession().createQuery("from User where accessCode=?").setParameter(0, accessCode).list();
		if (userList.size() > 0)
			return true;
		else
		 return false;*/
		List result;
		if(role.equals(QuesnrConstants.STUDENT)){
			String hql = "select * from user u " + " where u.access_code=? and u.student_id=?";
			SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(hql);
			query.setString(0, accessCode);
			query.setLong(1, id);
			result = query.list();
			
		}else{
			String hql = "select * from user u " + " where u.access_code=? and u.institute_id=?";
			SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(hql);
			query.setString(0, accessCode);
			query.setLong(1, id);
			result = query.list();
		}
		
		if(result.size() > 0)
			return true;
		else
			return false;
		
	}

	@Override
	public void updatePassword(String password,long id,String role) {
		// TODO Auto-generated method stub
		if(role.equals(QuesnrConstants.STUDENT)){
			String hql = "update user u set u.password=? " + " where u.student_id=?";
			Query query = this.sessionFactory.getCurrentSession().createSQLQuery(hql);
			query.setString(0, password);
			query.setLong(1, id);
			query.executeUpdate();
			
		}else{
			String hql = "update user u set u.password=? " + " where u.institute_id=?";
			Query query = this.sessionFactory.getCurrentSession().createSQLQuery(hql);
			query.setString(0, password);
			query.setLong(1, id);
			query.executeUpdate();
		}
	}

	@Override
	public void updateProfilePassword(long userId, String password) {
		// TODO Auto-generated method stub
		String hql = "update user u set u.password=? " + " where u.user_id=?";
		Query query = this.sessionFactory.getCurrentSession().createSQLQuery(hql);
		query.setString(0, password);
		query.setLong(1, userId);
		query.executeUpdate();
	}

}