/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Ruchira
 *
 */
package com.quesnr.registration.management.controller;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.quesnr.constants.QuesnrConstants;
import com.quesnr.exception.QuesnrException;
import com.quesnr.generic.model.City;
import com.quesnr.generic.model.Country;
import com.quesnr.generic.model.State;
import com.quesnr.generic.service.QuesnrGenericService;
import com.quesnr.user.management.model.User;
import com.quesnr.user.management.service.UserService;
import com.quesnr.util.email.service.EmailService;

@Controller
public class RegistrationController {
	
	private static final Logger logger = LogManager.getLogger(RegistrationController.class);
	

	@Autowired
	UserService userService;
	@Autowired
	EmailService emailService;
	@Autowired
	QuesnrGenericService quesnrGenericService;
	
	
	
	@RequestMapping(value = { "/", "/sign-in","/login"}, method = RequestMethod.GET)
	public String login(ModelMap model, @RequestParam(value = "error",required = false) String error,
			@ModelAttribute("user") User user,HttpServletRequest request, HttpServletResponse response) {
		logger.info("In entry method");
		if (error != null) {
			model.addAttribute("msg", QuesnrConstants.INVALID_USERNAME_PASSWORD_MESSAGE);
		}
		return "welcomePages/entry";
	}
	
	@RequestMapping(value = {"/signup" }, method = RequestMethod.GET)
	public String signup(ModelMap model, @RequestParam(value = "error",required = false) String error,
			@ModelAttribute("user") User user,HttpServletRequest request, HttpServletResponse response) {
		logger.info("In Signup method");
		List<String> countryList = userService.getCountryList();
		if(countryList!=null && countryList.size() > 0){
			model.addAttribute("countryList", countryList);
		}
		return "signup";
	}
	
	@RequestMapping(value = {"/doSignup" }, method = RequestMethod.POST)
	public String doSignup(ModelMap model, @RequestParam(value = "error",required = false) String error,
			@ModelAttribute("user") User user,HttpServletRequest request, HttpServletResponse response,@CookieValue("JSESSIONID") String cookie) {
		boolean acceptedTerms = true;
		boolean validateUser = validateUser(user);
		
		if(user != null && !user.isAgreeTerms()){
			logger.info("Terms and Conditions not accepted");
			acceptedTerms = false;
			model.addAttribute("msg", QuesnrConstants.PLEASE_ACCEPT_TERMS);
			return "welcomePages/registration-block";
		}
		
		if(validateUser){
			logger.info("User is Validated");
			user = setUserDetails(user, cookie);
			userService.createUser(user);
			try {
				emailService.sendEmail(user, user.getStudent().getEmail());
			} catch (QuesnrException e) {
				// TODO Auto-generated catch block
				logger.info("",e.getErrorCode()+ " " +e.getMessage());
				model.addAttribute("errorCode",e.getErrorCode());
				model.addAttribute("errorMessage", e.getMessage());
				return "welcomePages/error-page";
			}
			model.addAttribute("msg", QuesnrConstants.PLEASE_VERIFY_ACCOUNT);
			return "welcomePages/entry";
		}else{
			logger.info("User not Validated");
			model.addAttribute("msg", QuesnrConstants.PLEASE_FILL_ALL_FIELDS );
			return "welcomePages/registration-block";
		}
	
	}
	
	private boolean validateUser(User user) {
		logger.info("Validating the User");
		boolean isValidated = true;
		/*if(user.getUsername().equals(null) || user.getUsername().equals("")){
			isValidated = false;
		}
		if(user.getPassword().equals(null) || user.getPassword().equals("")){
			isValidated = false;
		}
		if(user.getFirstName().equals(null) || user.getLastName().equals("")){
			isValidated = false;
		}
		if(user.getEmail().equals(null) || user.getEmail().equals("")){
			isValidated = false;
		}
		if(user.getContactNo().equals(null) || user.getContactNo().equals("")){
			isValidated = false;
		}
	*/
		return isValidated;
	}

	/*@RequestMapping(value = { "/home" }, method = RequestMethod.GET)
	public String home(ModelMap model,@ModelAttribute("user") User user,HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			HttpSession session) {
		user = (User) session.getAttribute("userDetails");
		getPrincipal();
		model.addAttribute("userDetails", user);
		return "home";
	}*/

	private User setUserDetails(User user,String cookie ) {
		// TODO Auto-generated method stub
	/*	String ipAddress = getIpAddress();
		user.setEnabled(false);
		user.setVerified(false);
		user.setIpAddress(ipAddress);
		user.setjSessionId(cookie);
		user.setUpdatedBy(QuesnrConstants.ROLE_SUPER_ADMIN);
		user.setCreatedBy(QuesnrConstants.ROLE_SUPER_ADMIN);
		user.setRole(QuesnrConstants.SUPER_ADMIN);
		Date dateCreated = new Date();  
		user.setDateCreated(dateCreated);
		Date updatedDate = new Date();  
		user.setUpdatedDate(updatedDate);
		user.setLastLoggedInTime(updatedDate);
		UserRole userRole = new UserRole();
		userRole.setUserRole(QuesnrConstants.ROLE_SUPER_ADMIN);
		userRole.setRole(QuesnrConstants.SUPER_ADMIN);
		Set<UserRole> roles = new HashSet<UserRole>();
		roles.add(userRole);
		user.setUserRole(roles);
		//user.setAgreeTerms(false);
*/		return user;
	}

	private String getIpAddress() {
		// TODO Auto-generated method stub
		InetAddress ip = null;
		 try {
			ip = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ip.getHostAddress();
	}
 	
 	@RequestMapping(value="/logout", method = RequestMethod.GET)
	 public String logoutPage (ModelMap model,HttpServletRequest request,
	HttpServletResponse response,@ModelAttribute("user") User user,  @RequestParam(value = "error",required = false) String error) {
	 Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	 if (auth != null){ 
	 new SecurityContextLogoutHandler().logout(request, response, auth);
	 }
	 model.addAttribute("msg", QuesnrConstants.LOG_OUT_MESSAGE);
	 return login(model, error, user, request, response);
	 }
 	
 	@RequestMapping(value="/verifyUser", method = RequestMethod.GET)
 	public String verifyUser(@RequestParam(value="username")String username, ModelMap model){
 		logger.info("In verify User method");
		User user = userService.findUserByUserName(username);		
		if(user != null && !user.isVerified()){
			user.setVerified(true);
			user.setEnabled(true);
			userService.updateUser(user);
			model.addAttribute("msg", null);
		}else if(user != null && user.isVerified()){
			model.addAttribute("msg", "AlreadyVerified");
		}
 	
 		return "welcomePages/verifiedFile";
 	}
 	@RequestMapping(value="/register", method = RequestMethod.GET)
 	public String openRegistrationPage(ModelMap model, @RequestParam(value="role")String selectedRole, @ModelAttribute("user") User user,
 			HttpServletRequest request, HttpServletResponse response){
 		logger.info("Opening Registration Page with "+selectedRole+" role");
 		
 		List<Country> countryList = quesnrGenericService.loadCountryListFromDatabase();
 		List<State> stateList = quesnrGenericService.loadStateListFromDatabase();
 		List<City> cityList = quesnrGenericService.loadCityListFromDatabase();
 		
 		model.addAttribute("countryList",countryList);
 		model.addAttribute("stateList",stateList);
 		model.addAttribute("cityList",cityList);
 		
 		if(selectedRole != null && selectedRole.equals(QuesnrConstants.REGISTER_AS_STUDENT)){
 			model.addAttribute("role",QuesnrConstants.STUDENT);
 		}else if(selectedRole != null && selectedRole.equals(QuesnrConstants.REGISTER_AS_INSTITUTE)){
 			model.addAttribute("role",QuesnrConstants.INSTITUTE);
 		}
 		return "welcomePages/registration-block";
 	}
 	
 	@RequestMapping(value="/states", method = RequestMethod.GET,produces = "application/json")
 	public @ResponseBody List<State> searchStates(@RequestParam String country,HttpServletRequest request, HttpServletResponse response){
 		logger.info("In search state method");
 		List<State> stateList = new ArrayList<State>();
 		
 		System.out.println("in state method"+country);
 		long countryId = quesnrGenericService.findCountryIdByName(country);
 		if(countryId > 0 )
 			stateList = quesnrGenericService.findStates(countryId);

		return stateList;
 	}
 	
 	@RequestMapping(value="/cities", method = RequestMethod.GET,produces = "application/json")
 	public @ResponseBody List<City> searchCities(@RequestParam String state,HttpServletRequest request, HttpServletResponse response){
 		logger.info("In search state method");
 		List<City> cityList = new ArrayList<City>();
 		long stateId = quesnrGenericService.findStateIdByStateName(state);
 		if(stateId > 0)
 			cityList = quesnrGenericService.findCities(stateId);
 		
		return cityList;
 	}
	
 	
 	@ExceptionHandler({IOException.class, Exception.class})
    public ModelAndView handleIOException(Exception ex) {
        ModelAndView model = new ModelAndView("welcomePages/error-page");
 
        model.addObject("exception", ex.getMessage());
         
        return model;
    }
}
