package com.quesnr.questions.service.Impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.quesnr.questions.dao.QuestionGenericDao;
import com.quesnr.questions.model.AcademicYears;
import com.quesnr.questions.model.Answer;
import com.quesnr.questions.model.Chapter;
import com.quesnr.questions.model.ClassMaster;
import com.quesnr.questions.model.McqOptions;
import com.quesnr.questions.model.Question;
import com.quesnr.questions.model.QuestionType;
import com.quesnr.questions.model.SubjectMaster;
import com.quesnr.questions.service.QuestionGenericService;

@Service("QuestionGenericService")
@Transactional
public class QuestionGenericServiceImpl implements QuestionGenericService{

	@Autowired 
	QuestionGenericDao dao;
	
	@Override
	public List<QuestionType> getAllQuestionTypes() {
		return dao.getAllQuestionTypes();
	}

	@Override
	public List<AcademicYears> getAllAcademicYears() {		
		return dao.getAllAcademicYears();
	}

	@Override
	public Serializable saveAnswer(Answer answer) {
		return dao.saveAnswer(answer);
	}

	@Override
	public Question saveQuestion(Question question) {
		return dao.saveQuestion(question);		
	}

	@Override
	public void saveMCQ(McqOptions mcqOptions) {
		dao.saveMCQ(mcqOptions);	
	}

	@Override
	public List<ClassMaster> getAllClasses() {
		return dao.getAllClasses();
	}

	@Override
	public List<SubjectMaster> getSubjectsByBoardIdAndClassId(int boardId, int classId) {
		return dao.getSubjectsByBoardIdAndClassId(boardId,classId);
	}

	@Override
	public List<Chapter> getChapterListBySubjectId(int subjectId) {		
		return dao.getChapterListBySubjectId(subjectId);
	}

	@Override
	public SubjectMaster getSubjectBySubjectId(int subjectId) {		
		return dao.getSubjectBySubjectId(subjectId);
	}

	@Override
	public Chapter getChaptersByChapterId(int chapterId) {		
		return dao.getChaptersByChapterId(chapterId);
	}

}
