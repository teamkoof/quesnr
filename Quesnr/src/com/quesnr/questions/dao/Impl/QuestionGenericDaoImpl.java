package com.quesnr.questions.dao.Impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.quesnr.questions.dao.QuestionGenericDao;
import com.quesnr.questions.model.AcademicYears;
import com.quesnr.questions.model.Answer;
import com.quesnr.questions.model.Chapter;
import com.quesnr.questions.model.ClassMaster;
import com.quesnr.questions.model.McqOptions;
import com.quesnr.questions.model.Question;
import com.quesnr.questions.model.QuestionType;
import com.quesnr.questions.model.SubjectMaster;

@Repository("QuestionGenericDao")
@Transactional
public class QuestionGenericDaoImpl implements QuestionGenericDao {

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<QuestionType> getAllQuestionTypes() {

		List<QuestionType> questionTypes = new ArrayList<QuestionType>();

		questionTypes = sessionFactory.getCurrentSession().createQuery("from QuestionType").list();

		return questionTypes;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AcademicYears> getAllAcademicYears() {
		List<AcademicYears> academicYears = new ArrayList<AcademicYears>();

		academicYears = sessionFactory.getCurrentSession().createQuery("from AcademicYears").list();

		return academicYears;
	}
    
	@Override
	public Serializable saveAnswer(Answer answer) {	

		return this.sessionFactory.getCurrentSession().save(answer);			 
	}

	@Override
	public Question saveQuestion(Question question) {
		this.sessionFactory.getCurrentSession().save(question);	
		return question;
	}

	@Override
	public void saveMCQ(McqOptions mcqOptions) {
		this.sessionFactory.getCurrentSession().save(mcqOptions);	
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ClassMaster> getAllClasses() {
		return this.sessionFactory.getCurrentSession().createQuery("from ClassMaster").list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SubjectMaster> getSubjectsByBoardIdAndClassId(int boardId, int classId) {
		return this.sessionFactory.getCurrentSession().createQuery("from SubjectMaster where class_Id=? and board_Id=?").setParameter(0, classId).setParameter(1, boardId).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Chapter> getChapterListBySubjectId(int subjectId) {		
		return this.sessionFactory.getCurrentSession().createQuery("from Chapter where subject_Id=?").setParameter(0, subjectId).list();
	}

	@Override
	public SubjectMaster getSubjectBySubjectId(int subjectId) {
		return (SubjectMaster) this.sessionFactory.getCurrentSession().createQuery("from SubjectMaster where subject_Id=?").setParameter(0, subjectId).uniqueResult();
	}

	@Override
	public Chapter getChaptersByChapterId(int chapterId) {
		return (Chapter) this.sessionFactory.getCurrentSession().createQuery("from Chapter where chapter_Id=?").setParameter(0, chapterId).uniqueResult();
	}

}
