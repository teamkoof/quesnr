package com.quesnr.questions.dao;

import java.io.Serializable;
import java.util.List;

import com.quesnr.questions.model.AcademicYears;
import com.quesnr.questions.model.Answer;
import com.quesnr.questions.model.Chapter;
import com.quesnr.questions.model.ClassMaster;
import com.quesnr.questions.model.McqOptions;
import com.quesnr.questions.model.Question;
import com.quesnr.questions.model.QuestionType;
import com.quesnr.questions.model.SubjectMaster;

public interface QuestionGenericDao {

	public List<QuestionType> getAllQuestionTypes();

	public List<AcademicYears> getAllAcademicYears();

	public Serializable saveAnswer(Answer answer);

	public Question saveQuestion(Question question);

	public void saveMCQ(McqOptions mcqOptions);

	public List<ClassMaster> getAllClasses();

	public List<SubjectMaster> getSubjectsByBoardIdAndClassId(int boardId, int classId);

	public List<Chapter> getChapterListBySubjectId(int subjectId);

	public SubjectMaster getSubjectBySubjectId(int subjectId);

	public Chapter getChaptersByChapterId(int chapterId);

}
