package com.quesnr.questions.model;

public class BoardMaster {
	private long boardId;
	private String boardName;

	public BoardMaster() {
		super();
	}

	public BoardMaster(long boardId, String boardName) {
		super();
		this.boardId = boardId;
		this.boardName = boardName;
	}

	public long getBoardId() {
		return boardId;
	}

	public void setBoardId(long boardId) {
		this.boardId = boardId;
	}

	public String getBoardName() {
		return boardName;
	}

	public void setBoardName(String boardName) {
		this.boardName = boardName;
	}

	@Override
	public String toString() {
		return "Board [boardId=" + boardId + ", boardName=" + boardName + "]";
	}

}
