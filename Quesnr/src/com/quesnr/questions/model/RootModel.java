package com.quesnr.questions.model;

import java.util.List;

public class RootModel {
	private Question question;
	private QuestionType questionType;
	private List<McqOptions> mcqOptions;
	private List<Answer> answers;
	private int boardId;
	private int classId;
	private int subjectId;
	private int chapterId;
	private boolean status;
	private SubjectMaster subjectMaster;
	private Chapter chapter;
	
	public RootModel() {
		super();
	}

	public RootModel(Question question, QuestionType questionType, List<McqOptions> mcqOptions, List<Answer> answers,
			int boardId, int classId, int subjectId, int chapterId, boolean status, SubjectMaster subjectMaster,
			Chapter chapter) {
		super();
		this.question = question;
		this.questionType = questionType;
		this.mcqOptions = mcqOptions;
		this.answers = answers;
		this.boardId = boardId;
		this.classId = classId;
		this.subjectId = subjectId;
		this.chapterId = chapterId;
		this.status = status;
		this.subjectMaster = subjectMaster;
		this.chapter = chapter;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public QuestionType getQuestionType() {
		return questionType;
	}

	public void setQuestionType(QuestionType questionType) {
		this.questionType = questionType;
	}

	public List<McqOptions> getMcqOptions() {
		return mcqOptions;
	}

	public void setMcqOptions(List<McqOptions> mcqOptions) {
		this.mcqOptions = mcqOptions;
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	public int getBoardId() {
		return boardId;
	}

	public void setBoardId(int boardId) {
		this.boardId = boardId;
	}

	public int getClassId() {
		return classId;
	}

	public void setClassId(int classId) {
		this.classId = classId;
	}

	public int getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}

	public int getChapterId() {
		return chapterId;
	}

	public void setChapterId(int chapterId) {
		this.chapterId = chapterId;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public SubjectMaster getSubjectMaster() {
		return subjectMaster;
	}

	public void setSubjectMaster(SubjectMaster subjectMaster) {
		this.subjectMaster = subjectMaster;
	}

	public Chapter getChapter() {
		return chapter;
	}

	public void setChapter(Chapter chapter) {
		this.chapter = chapter;
	}

	@Override
	public String toString() {
		return "RootModel [question=" + question + ", questionType=" + questionType + ", mcqOptions=" + mcqOptions
				+ ", answers=" + answers + ", boardId=" + boardId + ", classId=" + classId + ", subjectId=" + subjectId
				+ ", chapterId=" + chapterId + ", status=" + status + ", subjectMaster=" + subjectMaster + ", chapter="
				+ chapter + "]";
	}

}
