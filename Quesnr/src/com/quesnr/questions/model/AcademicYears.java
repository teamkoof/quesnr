package com.quesnr.questions.model;

public class AcademicYears {
	private int yearId;
	private int year;
	public AcademicYears() {
		super();
		// TODO Auto-generated constructor stub
	}
	public AcademicYears(int yearId, int year) {
		super();
		this.yearId = yearId;
		this.year = year;
	}
	public int getYearId() {
		return yearId;
	}
	public void setYearId(int yearId) {
		this.yearId = yearId;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	
}
