package com.quesnr.questions.model;


public class McqOptions {
	private long optionId;
	private String optionName;
	private Question questionId;

	public McqOptions() {
		super();
	}

	public McqOptions(long optionId, String optionName, Question questionId) {
		super();
		this.optionId = optionId;
		this.optionName = optionName;
		this.questionId = questionId;
	}

	public long getOptionId() {
		return optionId;
	}

	public void setOptionId(long optionId) {
		this.optionId = optionId;
	}

	public String getOptionName() {
		return optionName;
	}

	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}

	public Question getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Question questionId) {
		this.questionId = questionId;
	}

	@Override
	public String toString() {
		return "McqOptions [optionId=" + optionId + ", optionName=" + optionName + ", questionId=" + questionId + "]";
	}

}
