package com.quesnr.questions.model;

import com.quesnr.generic.model.Board;

public class SubjectMaster {
	private long subjectId;
	private String subjectName;
	private ClassMaster classId;
	private Board boardId;

	public SubjectMaster() {
		super();
	}

	public SubjectMaster(long subjectId, String subjectName, ClassMaster classId, Board boardId) {
		super();
		this.subjectId = subjectId;
		this.subjectName = subjectName;
		this.classId = classId;
		this.boardId = boardId;
	}

	public long getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(long subjectId) {
		this.subjectId = subjectId;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public ClassMaster getClassId() {
		return classId;
	}

	public void setClassId(ClassMaster classId) {
		this.classId = classId;
	}

	public Board getBoardId() {
		return boardId;
	}

	public void setBoardId(Board boardId) {
		this.boardId = boardId;
	}

	@Override
	public String toString() {
		return "Subject [subjectId=" + subjectId + ", subjectName=" + subjectName + ", classId=" + classId
				+ ", boardId=" + boardId + "]";
	}


}
