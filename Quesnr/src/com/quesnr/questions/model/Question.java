package com.quesnr.questions.model;

import java.util.Set;

public class Question {
	private int questionId;
	private String question;
	private QuestionType questionTypeId;
	private Set<Answer> answer;
	private String level;
	private int mark;
	private int time;
	private SubjectMaster subjectId;
	private Chapter chapterId;
	private int year;
	private Boolean image;
	
	public Question() {
		super();
	}

	public Question(int questionId, String question, QuestionType questionTypeId, Set<Answer> answer, String level,
			int mark, int time, SubjectMaster subjectId, Chapter chapterId, int year,Boolean image) {
		super();
		this.questionId = questionId;
		this.question = question;
		this.questionTypeId = questionTypeId;
		this.answer = answer;
		this.level = level;
		this.mark = mark;
		this.time = time;
		this.subjectId = subjectId;
		this.chapterId = chapterId;
		this.year = year;
		this.image = image;
	}

	public int getQuestionId() {
		return questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public QuestionType getQuestionTypeId() {
		return questionTypeId;
	}

	public void setQuestionTypeId(QuestionType questionTypeId) {
		this.questionTypeId = questionTypeId;
	}

	public Set<Answer> getAnswer() {
		return answer;
	}

	public void setAnswer(Set<Answer> answer) {
		this.answer = answer;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public int getMark() {
		return mark;
	}

	public void setMark(int mark) {
		this.mark = mark;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public SubjectMaster getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(SubjectMaster subjectId) {
		this.subjectId = subjectId;
	}

	public Chapter getChapterId() {
		return chapterId;
	}

	public void setChapterId(Chapter chapterId) {
		this.chapterId = chapterId;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public Boolean getImage() {
		return image;
	}

	public void setImage(Boolean image) {
		this.image = image;
	}
	@Override
	public String toString() {
		return "Questions [questionId=" + questionId + ", question=" + question + ", questionTypeId=" + questionTypeId
				+ ", answerId=" + answer + ", level=" + level + ", mark=" + mark
				+ ", time=" + time + ", subjectId=" + subjectId + ", chapterId=" + chapterId + ", year=" + year + ", image=" + image+"]";
	}	
	
		
}
