package com.quesnr.questions.model;

public class Chapter {
	private long chapterId;
	private long chapterIndex;
	private String chapterName;
	private SubjectMaster subjectId;

	public Chapter() {
		super();
	}

	public Chapter(long chapterId, long chapterIndex, String chapterName, SubjectMaster subjectId) {
		super();
		this.chapterId = chapterId;
		this.chapterIndex = chapterIndex;
		this.chapterName = chapterName;
		this.subjectId = subjectId;
	}

	public long getChapterId() {
		return chapterId;
	}

	public void setChapterId(long chapterId) {
		this.chapterId = chapterId;
	}

	public long getChapterIndex() {
		return chapterIndex;
	}

	public void setChapterIndex(long chapterIndex) {
		this.chapterIndex = chapterIndex;
	}

	public String getChapterName() {
		return chapterName;
	}

	public void setChapterName(String chapterName) {
		this.chapterName = chapterName;
	}

	public SubjectMaster getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(SubjectMaster subjectId) {
		this.subjectId = subjectId;
	}

	@Override
	public String toString() {
		return "Chapter [chapterId=" + chapterId + ", chapterIndex=" + chapterIndex + ", chapterName=" + chapterName
				+ ", subjectId=" + subjectId + "]";
	}

}
