package com.quesnr.questions.model;

import java.util.Set;

public class QuestionType {

	private long typeId;
	private String type;
	private Set<Question> question;
	public QuestionType() {
		super();
	}
	public QuestionType(long typeId, String type, Set<Question> question) {
		super();
		this.typeId = typeId;
		this.type = type;
		this.question = question;
	}
	public long getTypeId() {
		return typeId;
	}
	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Set<Question> getQuestion() {
		return question;
	}
	public void setQuestion(Set<Question> question) {
		this.question = question;
	}
	@Override
	public String toString() {
		return "QuestionType [typeId=" + typeId + ", type=" + type + ", question=" + question + "]";
	}
	

}
