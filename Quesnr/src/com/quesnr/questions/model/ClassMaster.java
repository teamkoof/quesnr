package com.quesnr.questions.model;


public class ClassMaster {
	private long classId;
	private int classNum;
	private String classChar;

	public ClassMaster() {
		super();	
	}

	public ClassMaster(long classId, int classNum, String classChar) {
		super();
		this.classId = classId;
		this.classNum = classNum;
		this.classChar = classChar;
	}

	public long getClassId() {
		return classId;
	}

	public void setClassId(long classId) {
		this.classId = classId;
	}

	public int getClassNum() {
		return classNum;
	}

	public void setClassNum(int classNum) {
		this.classNum = classNum;
	}

	public String getClassChar() {
		return classChar;
	}

	public void setClassChar(String classChar) {
		this.classChar = classChar;
	}

	@Override
	public String toString() {
		return "Class [classId=" + classId + ", classNum=" + classNum + ", classChar=" + classChar + "]";
	}


}
