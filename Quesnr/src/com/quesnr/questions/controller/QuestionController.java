package com.quesnr.questions.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.quesnr.generic.service.QuesnrGenericService;
import com.quesnr.questions.model.AcademicYears;
import com.quesnr.questions.model.Answer;
import com.quesnr.questions.model.Chapter;
import com.quesnr.questions.model.ClassMaster;
import com.quesnr.questions.model.McqOptions;
import com.quesnr.questions.model.Question;
import com.quesnr.questions.model.QuestionType;
import com.quesnr.questions.model.RootModel;
import com.quesnr.questions.model.SubjectMaster;
import com.quesnr.questions.service.QuestionGenericService;

@Controller
public class QuestionController {
	
	@Autowired
	QuestionGenericService service;
	
	@Autowired
	QuesnrGenericService quesnrService;
	
	@RequestMapping(value= {"/addQuestions"},method=RequestMethod.GET)
	public String addQuestions(Model model) {		
		List<AcademicYears> yearList = service.getAllAcademicYears();
		List<QuestionType> typeList = service.getAllQuestionTypes();
		List<ClassMaster> classMasters = service.getAllClasses(); 
		model.addAttribute("boardList", quesnrService.loadBoardListFromDatabase());
		model.addAttribute("classList", classMasters);
		model.addAttribute("QuestionTypeList", typeList);
		model.addAttribute("AcademicYearsList", yearList);
		//model.addAttribute("question", question);
		return "addQuestions";
	}
	
	@RequestMapping(value= {"getClassesByBoardId"})
	public @ResponseBody List<SubjectMaster> getClassesByBoardId(@RequestParam("boardId")int boardId,@RequestParam("classId")int classId){
		System.out.println("BoardId : "+boardId);
		System.out.println("ClassId : "+classId);
		List<SubjectMaster> subjectMasters = service.getSubjectsByBoardIdAndClassId(boardId,classId);
		for (SubjectMaster subjectMaster : subjectMasters) {
			System.out.println("Subject Name : "+subjectMaster.getSubjectName());
		}
		return subjectMasters;
	}
	
	@RequestMapping(value= {"getChapterListBySubjectId"})
	public @ResponseBody List<Chapter> getChapterListBySubjectId(@RequestParam("subjectId")int subjectId){
		List<Chapter> chapters = service.getChapterListBySubjectId(subjectId);
		return chapters;
	}
	
	@RequestMapping(value= {"addQuestion"},method=RequestMethod.POST)
	public @ResponseBody String addQuestion(@RequestBody List<RootModel> rootModelList) throws InterruptedException {
		SubjectMaster subjectMaster ;
		Chapter chapter;
		System.out.println(rootModelList);
		for (RootModel rootModel : rootModelList) {
			System.out.println("RootModel : "+rootModel.toString());		
			QuestionType questionType = rootModel.getQuestionType();
			subjectMaster = getSubjectMaster(rootModel.getSubjectId());			
			chapter = getChapter(rootModel.getChapterId());	

			Question question = rootModel.getQuestion();
			question.setQuestionTypeId(questionType);
			question.setSubjectId(subjectMaster);
			question.setChapterId(chapter);
			Question questionId = service.saveQuestion(question);

			List<McqOptions> mcqOptions = rootModel.getMcqOptions();
			if(!(mcqOptions == null)) {
				for (McqOptions mcqOptions2 : mcqOptions) {
					mcqOptions2.setQuestionId(questionId);
					service.saveMCQ(mcqOptions2);
				}
			}
			List<Answer> answers = rootModel.getAnswers();
			for (Answer answer : answers) {
				answer.setQuestion(questionId);
				service.saveAnswer(answer);		
			}			
		}		
		return "true";
	}
	
	public SubjectMaster getSubjectMaster(int subjectId) {
		SubjectMaster subjectMaster = service.getSubjectBySubjectId(subjectId);
		return subjectMaster;
	}
	
	public Chapter getChapter(int chapterId) {
		Chapter chapter = service.getChaptersByChapterId(chapterId);
		return chapter;
	}

}
