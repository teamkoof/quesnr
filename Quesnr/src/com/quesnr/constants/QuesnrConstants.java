/* Copyright (C) Quesnr - All Rights Reserved
 * 
 */
/**
 * @author Ruchira
 *
 */
package com.quesnr.constants;

import java.text.SimpleDateFormat;

public class QuesnrConstants {
	
	public static final String NULL_VALUE = null;
	
	public static final String ROLE_STUDENT = "ROLE_STUDENT";
	public static final String ROLE_INSTITUTE = "ROLE_INSTITUTE";
	
	public static final String STUDENT = "Student";
	public static final String INSTITUTE = "Institute";
	
	public static final String ST = "ST";
	public static final String INST = "INST";
	
	public static final SimpleDateFormat DATE_CREATED = new SimpleDateFormat("dd-mm-yyyy hh:mm:ss"); 
	public static final SimpleDateFormat UPDATED_DATE = new SimpleDateFormat("dd-mm-yyyy hh:mm:ss");
	
	public static final String LOG_OUT_MESSAGE = "You've been logged out successfully.";
	public static final String INVALID_USERNAME_PASSWORD_MESSAGE = "Invalid username and password!";
	public static final String PLEASE_VERIFY_ACCOUNT = "Please check your email and verify your account before login!";
	public static final String RESET_PASSWORD = "Please check your email and reset your password!";
	public static final String PLEASE_FILL_ALL_FIELDS = "Please fill out all the fields!";
	public static final String PLEASE_ACCEPT_TERMS = "Please accept the terms and conditions before you proceed!";
	public static final String USERNAME_ALREADY_EXIST = "Username already exist!";
	
	public static final String REGISTER_AS_STUDENT = "student";
	public static final String REGISTER_AS_INSTITUTE = "institute";
	
	public static final String VERSION_CONTROL = "1.0.1";
}
