/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Ruchira
 *
 */
package com.quesnr.student.management.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.quesnr.student.management.dao.StudentDAO;
import com.quesnr.student.management.service.StudentService;
import com.quesnr.user.management.model.User;
import com.quesnr.user.management.service.impl.UserServiceImpl;
@Service("studentService")
@Transactional
public class StudentServiceImpl implements StudentService{
private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);
	
	@Autowired
	private StudentDAO studentDAO;
	@Override
	public void createStudentUser(User user) {
		// TODO Auto-generated method stub
		logger.info("In create Student user method..");
		studentDAO.createStudentUser(user);
	}
	@Override
	public User findUserByUserId(long userId) {
		// TODO Auto-generated method stub
		return studentDAO.findUserByUserId(userId);
	}
	@Override
	public User findUserByUserName(String username) {
		// TODO Auto-generated method stub
		return studentDAO.findUserByUserName(username);
	}
	@Override
	public User updateUser(User user) {
		// TODO Auto-generated method stub
		return studentDAO.updateUser(user);
	}
	@Override
	public List<User> getStudentUserList() {
		// TODO Auto-generated method stub
		return studentDAO.getStudentUserList();
	}
	
}
