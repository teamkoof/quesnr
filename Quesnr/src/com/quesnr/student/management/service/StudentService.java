/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Ruchira
 *
 */
package com.quesnr.student.management.service;

import java.util.List;

import com.quesnr.user.management.model.User;

public interface StudentService {
	public void createStudentUser(User user);
	public User findUserByUserId(long userId);
	public User findUserByUserName(String username);
	public User updateUser(User user);
	public List<User> getStudentUserList();
}
