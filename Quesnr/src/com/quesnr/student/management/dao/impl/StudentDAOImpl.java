/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Ruchira
 *
 */
package com.quesnr.student.management.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.quesnr.constants.QuesnrConstants;
import com.quesnr.student.management.dao.StudentDAO;
import com.quesnr.user.management.model.User;

@Repository("studentDAO")
@Transactional
public class StudentDAOImpl implements StudentDAO{
private static final Logger logger = LogManager.getLogger(StudentDAOImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	@Override
	public void createStudentUser(User user) {
		// TODO Auto-generated method stub
		
		logger.info("In create user dao method");
		this.sessionFactory.getCurrentSession().saveOrUpdate(user);	
	}
	@SuppressWarnings("unchecked")
	@Override
	public User findUserByUserId(long userId) {
		// TODO Auto-generated method stub
		List<User> userList = new ArrayList<User>();
		userList = this.sessionFactory.getCurrentSession().createQuery("from User where userId=?").setParameter(0, userId).list();

		if (userList.size() > 0)
			return userList.get(0);
		else
		 return null;
	}
	@SuppressWarnings("unchecked")
	@Override
	public User findUserByUserName(String username) {
		// TODO Auto-generated method stub
		List<User> userList = new ArrayList<User>();
		userList = this.sessionFactory.getCurrentSession().createQuery("from User where username=?").setParameter(0, username).list();

		if (userList.size() > 0)
			return userList.get(0);
		else
		 return null;
	}
	@Override
	public User updateUser(User user) {
		// TODO Auto-generated method stub
		this.sessionFactory.getCurrentSession().update(user);
		return user;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<User> getStudentUserList() {
		// TODO Auto-generated method stub
		List<User> userList = new ArrayList<User>();
		userList = this.sessionFactory.getCurrentSession().createQuery("from User where role=?").setParameter(0, QuesnrConstants.STUDENT).list();

		if (userList.size() > 0)
			return userList;
		else
		 return null;
	}
}
