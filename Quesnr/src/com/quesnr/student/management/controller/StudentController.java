/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Ruchira
 *
 */
package com.quesnr.student.management.controller;


import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.quesnr.constants.QuesnrConstants;
import com.quesnr.exception.QuesnrException;
import com.quesnr.generic.model.Board;
import com.quesnr.generic.service.QuesnrGenericService;
import com.quesnr.student.management.service.StudentService;
import com.quesnr.user.management.model.User;
import com.quesnr.user.management.model.UserRole;
import com.quesnr.util.HashCode;
import com.quesnr.util.QuesnrUtil;
import com.quesnr.util.email.service.EmailService;

@Controller
public class StudentController {
	
	@Autowired
	StudentService studentService;
	@Autowired
	EmailService emailService;
	@Autowired
	QuesnrGenericService quesnrGenericService;
	@Autowired
	HashCode hashCode;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	QuesnrUtil quesnrUtil;
	
	private static final Logger logger = LogManager.getLogger(StudentController.class);
	
	@RequestMapping(value = {"/studentRegistration" }, method = RequestMethod.POST)
	public String studentRegistration(ModelMap model, @RequestParam(value = "error",required = false) String error,
			@ModelAttribute("user") User user,HttpServletRequest request, HttpServletResponse response,@CookieValue("JSESSIONID") String cookie) {
		
		boolean acceptedTerms = true;
		boolean validateUser = validateUser(user);
		
		if(user != null && !user.isAgreeTerms()){
			logger.info("Terms and Conditions not accepted");
			acceptedTerms = false;
			model.addAttribute("msg", QuesnrConstants.PLEASE_ACCEPT_TERMS);
			model.addAttribute("role",QuesnrConstants.REGISTER_AS_STUDENT);
			return "welcomePages/registration-block";
		}
		
		if(validateUser){
			logger.info("User is Validated");
			User findUser = studentService.findUserByUserName(user.getUsername());
			if(findUser!=null){
				if(findUser.getUsername().equals(user.getUsername())){
					logger.info("User not Validated");
					model.addAttribute("msg", QuesnrConstants.USERNAME_ALREADY_EXIST );
					model.addAttribute("role",QuesnrConstants.REGISTER_AS_STUDENT);
					return "welcomePages/registration-block";
				}else{
					try {
						user = setStudentDetails(user, cookie);
					} catch (QuesnrException e1) {
						// TODO Auto-generated catch block
						logger.info("",e1.getErrorCode()+ " " +e1.getMessage());
						model.addAttribute("errorCode",e1.getErrorCode());
						model.addAttribute("An error occured while processing your request.", e1.getMessage());
						return "welcomePages/error-page";
					}
					studentService.createStudentUser(user);
					try {
						emailService.sendEmail(user, user.getStudent().getEmail());
					} catch (QuesnrException e) {
						// TODO Auto-generated catch block
						logger.info("",e.getErrorCode()+ " " +e.getMessage());
						model.addAttribute("errorCode",e.getErrorCode());
						model.addAttribute("An error occured while processing your request.", e.getMessage());
						return "welcomePages/error-page";
					}
					
					model.addAttribute("msg", QuesnrConstants.PLEASE_VERIFY_ACCOUNT);
					return "welcomePages/entry";
				}
			}else{
				try {
					user = setStudentDetails(user, cookie);
				} catch (QuesnrException e1) {
					// TODO Auto-generated catch block
					logger.info("",e1.getErrorCode()+ " " +e1.getMessage());
					model.addAttribute("errorCode",e1.getErrorCode());
					model.addAttribute("An error occured while processing your request.", e1.getMessage());
					return "welcomePages/error-page";
				}
				studentService.createStudentUser(user);
				try {
					emailService.sendEmail(user, user.getStudent().getEmail());
				} catch (QuesnrException e) {
					// TODO Auto-generated catch block
					logger.info("",e.getErrorCode()+ " " +e.getMessage());
					model.addAttribute("errorCode",e.getErrorCode());
					model.addAttribute("An error occured while processing your request.", e.getMessage());
					return "welcomePages/error-page";
				}
				
				model.addAttribute("msg", QuesnrConstants.PLEASE_VERIFY_ACCOUNT);
				return "welcomePages/entry";
			}
		}else{
			logger.info("User not Validated");
			model.addAttribute("msg", QuesnrConstants.PLEASE_FILL_ALL_FIELDS );
			model.addAttribute("role",QuesnrConstants.REGISTER_AS_STUDENT);
			return "welcomePages/registration-block";
		}
		
	}
	
	private User setStudentDetails(User user,String cookie ) throws QuesnrException {
		// TODO Auto-generated method stub
		String ipAddress = quesnrUtil.getIpAddress();
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		user.setEnabled(false);
		user.setVerified(false);
		user.setIpAddress(ipAddress);
		user.setjSessionId(cookie);
		user.setUpdatedBy(QuesnrConstants.ROLE_STUDENT);
		user.setCreatedBy(QuesnrConstants.ROLE_STUDENT);
		user.setRole(QuesnrConstants.STUDENT);
		Date dateCreated = new Date();  
		user.setDateCreated(dateCreated);
		Date updatedDate = new Date();  
		user.setUpdatedDate(updatedDate);
		user.setLastLoggedInTime(updatedDate);
		UserRole userRole = new UserRole();
		userRole.setUserRole(QuesnrConstants.ROLE_STUDENT);
		userRole.setRole(QuesnrConstants.ROLE_STUDENT);
		Set<UserRole> roles = new HashSet<UserRole>();
		roles.add(userRole);
		user.setUserRole(roles);
		user.setBasicDetails(false);
		
		return user;
	}
	
	private boolean validateUser(User user) {
		logger.info("Validating the User");
		boolean isValidated = true;
		if(user.getUsername().equals(null) || user.getUsername().equals("")){
			isValidated = false;
		}
		if(user.getPassword().equals(null) || user.getPassword().equals("")){
			isValidated = false;
		}
		if(user.getStudent().getFirstName().equals(null) || user.getStudent().getLastName().equals("")){
			isValidated = false;
		}
		if(user.getStudent().getEmail().equals(null) || user.getStudent().getEmail().equals("")){
			isValidated = false;
		}
		if(user.getStudent().getContactNo().equals(null) || user.getStudent().getContactNo().equals("")){
			isValidated = false;
		}
	
		return isValidated;
	}
	
	@RequestMapping(value = {"/complete-registration" }, method = RequestMethod.POST)
	public String completeRegistration(ModelMap model, @ModelAttribute("user") User user,HttpServletRequest request, HttpServletResponse response, @CookieValue("JSESSIONID") String cookie, HttpSession session){
		
		User getUser = (User) session.getAttribute("userDetails");
		boolean basicDetailsAdded = false;
		
		if(user.getStudent()!=null){
			if(user.getStudent().getBoard().getBoardId() != 0){
				getUser.getStudent().getBoard().setBoardId(user.getStudent().getBoard().getBoardId());
				Board board = quesnrGenericService.getBoardByBoardId(user.getStudent().getBoard().getBoardId());
				getUser.getStudent().setBoard(board);
				basicDetailsAdded = true;
			}
			if(user.getStudent().getStudentClass() != 0){
				getUser.getStudent().setStudentClass(user.getStudent().getStudentClass());
				basicDetailsAdded = true;
			}
			if(user.getStudent().getSchool() != null){
				getUser.getStudent().setSchool(user.getStudent().getSchool());
				basicDetailsAdded = true;
			}
			if(user.getStudent().getSchoolCity() != null){
				getUser.getStudent().setSchoolCity(user.getStudent().getSchoolCity());
				basicDetailsAdded = true;
			}
			if(user.getStudent().getSchoolState() != null){
				getUser.getStudent().setSchoolState(user.getStudent().getSchoolState());
				basicDetailsAdded = true;
			}
			
		}
		
		if(basicDetailsAdded){
			getUser.setBasicDetails(basicDetailsAdded);
			logger.info("Adding Student Details and setting isBasicDetails to True");
			
			studentService.updateUser(getUser);
			return "welcomePages/entry";
		}else{
			return "welcomePages/after-login-details";
		}
			
	}
	
	@ExceptionHandler({IOException.class, Exception.class})
    public ModelAndView handleIOException(Exception ex) {
        ModelAndView model = new ModelAndView("welcomePages/error-page");
 
        model.addObject("An error occured while processing your request.", ex.getMessage());
         
        return model;
    }
	
}
