/** 
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Ruchira
 *
 */

package com.quesnr.detail.service;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.quesnr.user.management.model.UserRole;
import com.quesnr.user.management.service.UserService;

@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

	
	@Autowired
	private UserService userService;
	
	@Transactional(readOnly=true)
	@Override
	public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
		com.quesnr.user.management.model.User user = userService.findUserByUserName(username);
		if(user==null){
			System.out.println("User not found");
			throw new UsernameNotFoundException("Username not found"); 
		}
		/*List<GrantedAuthority> authorities = buildUserAuthority(user.getUserRole());
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		return buildUserForAuthentication(user, authorities);*/
		
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), 
				 user.isEnabled(), true, true, true, getGrantedAuthorities(user));
	}
	
	private List<GrantedAuthority> getGrantedAuthorities(com.quesnr.user.management.model.User user){
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		
		for(UserRole userRole : user.getUserRole()){
			System.out.println("UserProfile : "+userRole);
			authorities.add(new SimpleGrantedAuthority("ROLE_"+userRole.getRole()));
		}
		System.out.print("authorities :"+authorities);
		return authorities;
	}
	
	
 
}