/** 
 * Copyright (C) Quesnr - All Rights Reserved
 */
/**
 * @author Ruchira
 *
 */
package com.quesnr.detail.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.quesnr.user.management.service.UserService;

@Component
public class QuesnrEventHandler implements ApplicationListener<ContextRefreshedEvent>{
	private static final Logger logger = LogManager.getLogger(QuesnrEventHandler.class);
	@Autowired
	UserService userService;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent arg0) {
		// TODO Auto-generated method stub
		logger.info("application event...");
	}

}
