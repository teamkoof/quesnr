/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Ruchira
 *
 */
package com.quesnr.detail.service;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.quesnr.generic.model.Board;
import com.quesnr.generic.model.City;
import com.quesnr.generic.model.State;
import com.quesnr.generic.service.QuesnrGenericService;
import com.quesnr.user.management.service.UserService;

public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler{
	
	private static final Logger logger = LogManager.getLogger(CustomAuthenticationSuccessHandler.class);
	
	@Autowired
	UserService userService;
	@Autowired
	QuesnrGenericService quesnrGenericService;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		logger.info("In CustomAuthenticationSuccessHandler - onAuthenticationSuccess() method.");
		 HttpSession session = httpServletRequest.getSession();
	        User authUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	        session.setAttribute("username", authUser.getUsername());
	        session.setAttribute("authorities", authentication.getAuthorities());
	        com.quesnr.user.management.model.User userDetails = userService.findUserByUserName(authUser.getUsername());
	        com.quesnr.user.management.model.UserRole userRole= userService.findUserRoleByUserId(userDetails.getUserId());
	        Date lastLoggedInTime = new Date(); 
	        userDetails.setRole(userRole.getRole());
	        userDetails.setLastLoggedInTime(lastLoggedInTime);
	        //set our response to OK status
	        long loginCount= userDetails.getLoginCount() + 1;
	        userDetails.setLoginCount(loginCount);
	        userService.createUser(userDetails);
	        userDetails.setRole(userRole.getRole());
	        httpServletResponse.setStatus(HttpServletResponse.SC_OK);
	        session.setAttribute("userDetails", userDetails);
	        session.setAttribute("userRole", userRole);
	        
	        //loading list of board in session
	        List<Board> boardList = new ArrayList<Board>();
	        boardList = quesnrGenericService.loadBoardListFromDatabase();
	        if(boardList != null){
	        	session.setAttribute("boardList", boardList);
	        }
	        
	      //loading indian state and city in session
	        List<State> stateList = quesnrGenericService.loadIndianStateListFromDatabase();
	 		List<City> cityList = quesnrGenericService.loadCityListFromDatabase();
	 		
	 		session.setAttribute("stateList",stateList);
	 		session.setAttribute("cityList",cityList);
	        
	        
	        //since we have created our custom success handler, its up to us to where
	        //we will redirect the user after successfully login
	        httpServletResponse.sendRedirect("home");
	}

}
