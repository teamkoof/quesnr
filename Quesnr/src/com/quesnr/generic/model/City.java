/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Ruchira
 *
 */
package com.quesnr.generic.model;
public class City {
	private long cityId;
	private String cityName;
	private long stateId;
	
	/**
	 * @return the cityId
	 */
	public long getCityId() {
		return cityId;
	}
	/**
	 * @param cityId the cityId to set
	 */
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}
	/**
	 * @return the cityName
	 */
	public String getCityName() {
		return cityName;
	}
	/**
	 * @param cityName the cityName to set
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	/**
	 * @return the stateIId
	 */
	public long getStateId() {
		return stateId;
	}
	/**
	 * @param stateIId the stateIId to set
	 */
	public void setStateId(long stateId) {
		this.stateId = stateId;
	}
	
}
