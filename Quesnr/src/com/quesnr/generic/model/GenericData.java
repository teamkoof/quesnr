/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Ruchira
 *
 */
package com.quesnr.generic.model;
public class GenericData {
	private String country;
	private String state;
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
}
