/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Raviraj
 *
 */
package com.quesnr.generic.model;


public class Board {

	private long boardId;
	private String boardName;
	private String boardCode;
	private String boardState;
	
	public long getBoardId() {
		return boardId;
	}
	public void setBoardId(long boardId) {
		this.boardId = boardId;
	}
	public String getBoardName() {
		return boardName;
	}
	public void setBoardName(String boardName) {
		this.boardName = boardName;
	}
	public String getBoardCode() {
		return boardCode;
	}
	public void setBoardCode(String boardCode) {
		this.boardCode = boardCode;
	}
	public String getBoardState() {
		return boardState;
	}
	public void setBoardState(String boardState) {
		this.boardState = boardState;
	}
	
	
}
