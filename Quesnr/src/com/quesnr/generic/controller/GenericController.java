/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Raviraj
 *
 */
package com.quesnr.generic.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.quesnr.constants.QuesnrConstants;
import com.quesnr.registration.management.controller.RegistrationController;
import com.quesnr.student.management.service.StudentService;
import com.quesnr.user.management.model.Student;
import com.quesnr.user.management.model.User;
import com.quesnr.user.management.model.UserRole;
import com.quesnr.user.management.service.UserService;

@Controller
public class GenericController {

	private static final Logger logger = LogManager.getLogger(RegistrationController.class);
	
	@Autowired
	UserService userService;
	
	@Autowired
	StudentService studentService;
	
	@RequestMapping(value={"/settings"}, method = RequestMethod.GET)
	public String settings(ModelMap model,@ModelAttribute("user") User user,@ModelAttribute("displayUser") User displayUser, 
			@ModelAttribute("student")Student student, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			HttpSession session) {
		logger.info("Redirecting to settings page");
		user = (User) session.getAttribute("userDetails");
		  
		UserRole userRole = (UserRole) session.getAttribute("userRole");
		
		if(userRole.getRole().equals(QuesnrConstants.ROLE_STUDENT)){
			displayUser.setName(user.getStudent().getFirstName().concat(" ").concat(user.getStudent().getLastName()));
			displayUser.setEmail(user.getStudent().getEmail());
			displayUser.setContactNo(user.getStudent().getContactNo());
			model.addAttribute("role", QuesnrConstants.STUDENT);
		}else if(userRole.getRole().equals(QuesnrConstants.ROLE_INSTITUTE)){
			displayUser.setName(user.getInstitute().getInstituteName());
			displayUser.setEmail(user.getInstitute().getEmail());
			displayUser.setContactNo(user.getInstitute().getContactNo());
			model.addAttribute("role", QuesnrConstants.INSTITUTE);
		}
		
		model.addAttribute("user", user);
		model.addAttribute("displayUser", displayUser);
		
	    return "settings";
				
	}
	
	@RequestMapping(value={"/changeSettings"}, method = RequestMethod.POST)
	public String changeSettings(ModelMap model, @ModelAttribute("user") User user,@ModelAttribute("displayUser") User displayUser, 
			@ModelAttribute("student")Student student, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			HttpSession session) {
		logger.info("Changing the settings");
		user = (User) session.getAttribute("userDetails");

		
		//user.setThemeColor(themeColor);
		
		
		UserRole userRole = (UserRole) session.getAttribute("userRole");
		
		if(userRole.getRole().equals(QuesnrConstants.ROLE_STUDENT)){
			displayUser.setName(user.getStudent().getFirstName().concat(" ").concat(user.getStudent().getLastName()));
			displayUser.setEmail(user.getStudent().getEmail());
			displayUser.setContactNo(user.getStudent().getContactNo());
			model.addAttribute("role", QuesnrConstants.STUDENT);
		}else if(userRole.getRole().equals(QuesnrConstants.ROLE_INSTITUTE)){
			displayUser.setName(user.getInstitute().getInstituteName());
			displayUser.setEmail(user.getInstitute().getEmail());
			displayUser.setContactNo(user.getInstitute().getContactNo());
			model.addAttribute("role", QuesnrConstants.INSTITUTE);
		}
		
		model.addAttribute("user", user);
		model.addAttribute("displayUser", displayUser);
		
	    return "settings";
				
	}
	
	@ExceptionHandler({IOException.class, Exception.class})
    public ModelAndView handleIOException(Exception ex) {
        ModelAndView model = new ModelAndView("welcomePages/error-page");
 
        model.addObject("exception", ex.getMessage());
         
        return model;
    }
}
