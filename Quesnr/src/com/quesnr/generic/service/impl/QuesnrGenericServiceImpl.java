/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Raviraj
 *
 */
package com.quesnr.generic.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.quesnr.generic.dao.QuesnrGenericDao;
import com.quesnr.generic.model.Board;
import com.quesnr.generic.model.City;
import com.quesnr.generic.model.Country;
import com.quesnr.generic.model.State;
import com.quesnr.generic.service.QuesnrGenericService;

@Service("quesnrGenericService")
@Transactional
public class QuesnrGenericServiceImpl implements QuesnrGenericService {

	private static final Logger logger = LogManager.getLogger(QuesnrGenericServiceImpl.class);
	
	@Autowired
	QuesnrGenericDao quesnrGenericDao;
	
	@Override
	public List<Board> loadBoardListFromDatabase() {
		
		logger.info("Loading Board List");
		List<Board> boardList = new ArrayList<Board>();
		boardList = quesnrGenericDao.loadBoardListFromDatabase();
		
		if(boardList != null){
			return boardList;
		}else{
			return null;
		}
		
	}

	@Override
	public Board getBoardByBoardId(long boardId) {
		logger.info("Getting Board for board_id "+boardId);
		Board board = quesnrGenericDao.getBoardByBoardId(boardId);
		return board;
	}
	
	@Override
	public List<Country> loadCountryListFromDatabase() {
		// TODO Auto-generated method stub
		return quesnrGenericDao.loadCountryListFromDatabase();
	}

	@Override
	public List<State> loadStateListFromDatabase() {
		// TODO Auto-generated method stub
		return quesnrGenericDao.loadStateListFromDatabase();
	}

	@Override
	public List<City> loadCityListFromDatabase() {
		// TODO Auto-generated method stub
		return quesnrGenericDao.loadCityListFromDatabase();
	}

	@Override
	public List<State> findStates(long countryId) {
		// TODO Auto-generated method stub
		return quesnrGenericDao.findStates(countryId);
	}

	@Override
	public List<City> findCities(long stateId) {
		// TODO Auto-generated method stub
		return quesnrGenericDao.findCities(stateId);
	}

	@Override
	public long findCountryIdByName(String countryName) {
		// TODO Auto-generated method stub
		return quesnrGenericDao.findCountryIdByName(countryName);
	}

	@Override
	public long findStateIdByStateName(String stateName) {
		// TODO Auto-generated method stub
		return quesnrGenericDao.findStateIdByStateName(stateName);
	}

	@Override
	public List<State> loadIndianStateListFromDatabase() {
		// TODO Auto-generated method stub
		return quesnrGenericDao.loadIndianStateListFromDatabase();
	}

}
