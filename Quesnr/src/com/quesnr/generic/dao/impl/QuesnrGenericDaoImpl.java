/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Raviraj
 *
 */
package com.quesnr.generic.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.quesnr.generic.dao.QuesnrGenericDao;
import com.quesnr.generic.model.Board;
import com.quesnr.generic.model.City;
import com.quesnr.generic.model.Country;
import com.quesnr.generic.model.State;


@Repository("quesnrGenericDao")
@Transactional
public class QuesnrGenericDaoImpl implements QuesnrGenericDao {

	private static final Logger logger = LogManager.getLogger(QuesnrGenericDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<Board> loadBoardListFromDatabase() {
		
		List<Board> boardList = new ArrayList<Board>();
		
		boardList = sessionFactory.getCurrentSession().createQuery("from Board").list();
		
		if(boardList != null){
			logger.info("board list loaded successfully");
			return boardList;
		}else{
			return null;
		}
		
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public Board getBoardByBoardId(long boardId) {
		
		List<Board> boardList = new ArrayList<Board>();
		
		boardList = this.sessionFactory.getCurrentSession().createQuery("from Board where board_id=?").setParameter(0, boardId).list();
		
		if (boardList.size() > 0){
			return boardList.get(0);
		}else{
			 return null;
		}
		
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<Country> loadCountryListFromDatabase() {
		// TODO Auto-generated method stub
		List<Country> countryList = new ArrayList<Country>();
		countryList = this.sessionFactory.getCurrentSession().createQuery("from Country").list();
		
		if (countryList.size() > 0)
			return countryList;
		else
		 return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<State> loadStateListFromDatabase() {
		// TODO Auto-generated method stub
		List<State> stateList = new ArrayList<State>();
		stateList = this.sessionFactory.getCurrentSession().createQuery("from State").list();
		
		if (stateList.size() > 0)
			return stateList;
		else
		 return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<City> loadCityListFromDatabase() {
		// TODO Auto-generated method stub
		List<City> cityList = new ArrayList<City>();
		cityList = this.sessionFactory.getCurrentSession().createQuery("from City").list();
		
		if (cityList.size() > 0)
			return cityList;
		else
		 return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<State> findStates(long countryId) {
		// TODO Auto-generated method stub
		//long country =  Long.parseLong(countryId);
		List<State> stateList = new ArrayList<State>();
		stateList = this.sessionFactory.getCurrentSession().createQuery("from State where countryId=?").setParameter(0, countryId).list();
		if (stateList.size() > 0)
			return stateList;
		else
		 return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<City> findCities(long stateId) {
		// TODO Auto-generated method stub
		//long state =  Long.parseLong(stateId);
		List<City> cityList = new ArrayList<City>();
		cityList = this.sessionFactory.getCurrentSession().createQuery("from City where stateId=?").setParameter(0, stateId).list();
		if (cityList.size() > 0)
			return cityList;
		else
		 return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public long findCountryIdByName(String countryName) {
		// TODO Auto-generated method stub
		/*String sql = "SELECT country_id FROM country";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(sql);
		query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		List results = query.list();*/
		
		
		String hql = "SELECT c.countryId FROM Country c where c.countryName =:countryName";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("countryName", countryName);
		List results = query.list();
		if(results.size() > 0)
			return (long) results.get(0);
		return 0;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public long findStateIdByStateName(String stateName) {
		// TODO Auto-generated method stub
		String hql = "SELECT s.stateId FROM State s where s.stateName=:stateName";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("stateName", stateName);
		List results = query.list();
		if(results.size() > 0)
			return (long) results.get(0);
		return 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<State> loadIndianStateListFromDatabase() {
		// TODO Auto-generated method stub
		List<State> stateList = new ArrayList<State>();
		stateList = this.sessionFactory.getCurrentSession().createQuery("from State where countryId=?").setParameter(0, Long.valueOf(101)).list();
		if (stateList.size() > 0)
			return stateList;
		else
		 return null;
	}

}
