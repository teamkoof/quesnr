/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Raviraj
 *
 */
package com.quesnr.generic.dao;

import java.util.List;

import com.quesnr.generic.model.Board;
import com.quesnr.generic.model.City;
import com.quesnr.generic.model.Country;
import com.quesnr.generic.model.State;

public interface QuesnrGenericDao {

	List<Board> loadBoardListFromDatabase();

	public Board getBoardByBoardId(long boardId);
	
	List<Country> loadCountryListFromDatabase();
	List<State> loadStateListFromDatabase();
	List<City> loadCityListFromDatabase();
	List<State> findStates(long countryId);
	List<City> findCities(long stateId);
	long findCountryIdByName(String countryName);
	long findStateIdByStateName(String stateName);
	List<State> loadIndianStateListFromDatabase();

}
