/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Ruchira
 *
 */
package com.quesnr.util.email.service;
import com.quesnr.exception.QuesnrException;
import com.quesnr.user.management.model.User;

public interface EmailService {
	public void sendEmail(User user,String email) throws QuesnrException;
	public void sendForgotPasswordEmailNotification(User user,long id,String accessCode,String role) throws QuesnrException;
}
