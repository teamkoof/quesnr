/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Ruchira
 *
 */
package com.quesnr.util.email.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.velocity.app.VelocityEngine;
import org.hibernate.QueryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.quesnr.constants.QuesnrConstants;
import com.quesnr.exception.QuesnrException;
import com.quesnr.user.management.model.User;
import com.quesnr.util.email.service.EmailService;

@Service("emailService")
public class EmailServiceImpl implements EmailService{
	
	private static final Logger logger = LogManager.getLogger(EmailServiceImpl.class);
	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	private VelocityEngine velocityEngine;
	@Override
	public void sendEmail(User user,String email) throws QuesnrException {
		// TODO Auto-generated method stub
		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper;
		 // attachments
        //String[] attachFiles = new String[1];
       // attachFiles[0] = "E:/quesnr.png";
        
		try {
			helper = new MimeMessageHelper(message,MimeMessageHelper.MULTIPART_MODE_RELATED,"UTF-8");
			
			helper.setFrom("Administrator");
			helper.setTo(email);
			helper.setSubject("Quesnr - Registration Confirmation");
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("username", user.getUsername());
			if(user.getRole().equals(QuesnrConstants.STUDENT)){
				params.put("name", user.getStudent().getFirstName());
			}else{
				params.put("name", user.getInstitute().getInstituteName());
			}
			
			
			Multipart multipart = new MimeMultipart();
			
			MimeBodyPart mimeBody = new MimeBodyPart();
			String body = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "emailconfig/emailRegistration.vm", "UTF-8", params);
			mimeBody.setContent(body, "text/html");
				        
			
			/*MimeBodyPart mimeImage = new MimeBodyPart();
			DataSource ds = new FileDataSource(new File("E:/quesnr.png"));
			mimeImage.setDataHandler(new DataHandler(ds));
			mimeImage.setDisposition(Part.INLINE);
			mimeImage.setContentID("<quesnrlogo>");
			mimeImage.addHeader("Content-Type", "image/png");*/
			
			/**	raj 18 Feb 2018
			 * We dont need to attach anything to the email*/	
			/*MimeBodyPart attachment = new MimeBodyPart(); 
			if (attachFiles != null && attachFiles.length > 0) {
	            for (String filePath : attachFiles) {
					DataSource source = new FileDataSource(filePath);
					attachment.setDataHandler(new DataHandler(source));
					attachment.setFileName(filePath);
					multipart.addBodyPart(attachment);
	            }
	        }*/
			
			multipart.addBodyPart(mimeBody);
			//multipart.addBodyPart(mimeImage);
			
			logger.info("Setting Content and Sending Email");
		    message.setContent(multipart);
			mailSender.send(message);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			throw new QueryException(e.getMessage());
		}
		
	}
	@Override
	public void sendForgotPasswordEmailNotification(User user,long id,String accessCode,String role) throws QuesnrException {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				MimeMessage message = mailSender.createMimeMessage();
				MimeMessageHelper helper;
				 // attachments
		       // String[] attachFiles = new String[1];
		       // attachFiles[0] = "E:/quesnr.png";
		        
				try {
					helper = new MimeMessageHelper(message,MimeMessageHelper.MULTIPART_MODE_RELATED,"UTF-8");
					
					helper.setFrom("Administrator");
					helper.setTo(user.getEmail());
					helper.setSubject("Quesnr - Reset Password");
					Map<String, Object> params = new HashMap<String, Object>();
					
					params.put("email", user.getEmail());
					params.put("id", id);
					params.put("accessCode", accessCode);
					if(role.equals(QuesnrConstants.STUDENT)){
						params.put("role", QuesnrConstants.STUDENT);
					}else{
						params.put("role", QuesnrConstants.INSTITUTE);
					}
					
					Multipart multipart = new MimeMultipart();
					
					MimeBodyPart mimeBody = new MimeBodyPart();
					String body = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "emailconfig/forgotPassword.vm", "UTF-8", params);
					mimeBody.setContent(body, "text/html");
					
					multipart.addBodyPart(mimeBody);
					//multipart.addBodyPart(mimeImage);
					
					logger.info("Setting Content and Sending Email");
				    message.setContent(multipart);
					mailSender.send(message);
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					throw new QuesnrException("Error Occured while sending an email", e.getMessage());
				}
	}

}
