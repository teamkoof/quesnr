/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Ruchira
 *
 */
package com.quesnr.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Random;

import com.quesnr.exception.QuesnrException;

public class QuesnrUtil {
	private static final String CHAR_LIST = 
	        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	
	private static final int RANDOM_STRING_LENGTH = 10;
	    
	public String generateAccessCode(){
		 StringBuffer randStr = new StringBuffer();
	        for(int i=0; i<RANDOM_STRING_LENGTH; i++){
	            int number = getRandomNumber();
	            char ch = CHAR_LIST.charAt(number);
	            randStr.append(ch);
	        }
	        return randStr.toString();
	}
	
	private int getRandomNumber() {
        int randomInt = 0;
        Random randomGenerator = new Random();
        randomInt = randomGenerator.nextInt(CHAR_LIST.length());
        if (randomInt - 1 == -1) {
            return randomInt;
        } else {
            return randomInt - 1;
        }
    }
	
	public String getIpAddress() throws QuesnrException {
		// TODO Auto-generated method stub
		InetAddress ip = null;
		 try {
			ip = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			throw new QuesnrException("Error occured in QuesnrUtil in getIpAddress() method ", e.getMessage());
		}
		return ip.getHostAddress();
	}
}
