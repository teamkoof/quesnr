/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Ruchira
 *
 */
package com.quesnr.institute.management.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.quesnr.institute.management.dao.InstituteDAO;
import com.quesnr.institute.management.service.InstituteService;
import com.quesnr.user.management.model.User;

@Service("instituteService")
@Transactional
public class InstituteServiceImpl implements InstituteService{

private static final Logger logger = LogManager.getLogger(InstituteServiceImpl.class);
	
	@Autowired
	private InstituteDAO instituteDAO;
	@Override
	public void createInstitute(User user) {
		// TODO Auto-generated method stub
		logger.info("In create Student user method..");
		instituteDAO.createInstitute(user);
	}
	@Override
	public User findUserByUserId(long userId) {
		// TODO Auto-generated method stub
		return instituteDAO.findUserByUserId(userId);
	}
	@Override
	public User findUserByUserName(String username) {
		// TODO Auto-generated method stub
		return instituteDAO.findUserByUserName(username);
	}
	@Override
	public User updateUser(User user) {
		// TODO Auto-generated method stub
		return instituteDAO.updateUser(user);
	}
	@Override
	public List<User> getInstituteList() {
		// TODO Auto-generated method stub
		return instituteDAO.getInstituteList();
	}

}
