/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Ruchira
 *
 */
package com.quesnr.institute.management.service;
import java.util.List;

import com.quesnr.user.management.model.User;

public interface InstituteService {
	public void createInstitute(User user);
	public User findUserByUserId(long userId);
	public User findUserByUserName(String username);
	public User updateUser(User user);
	public List<User> getInstituteList();
}
