/**
 * Copyright (C) Quesnr - All Rights Reserved
 */

/**
 * @author Ruchira
 *
 */
package com.quesnr.institute.management.controller;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.quesnr.constants.QuesnrConstants;
import com.quesnr.exception.QuesnrException;
import com.quesnr.institute.management.service.InstituteService;
import com.quesnr.user.management.model.User;
import com.quesnr.user.management.model.UserRole;
import com.quesnr.util.HashCode;
import com.quesnr.util.QuesnrUtil;
import com.quesnr.util.email.service.EmailService;

@Controller
public class InstituteController {

	@Autowired
	InstituteService instituteService;
	@Autowired
	EmailService emailService;
	@Autowired
	HashCode hashCode;
	@Autowired
	QuesnrUtil quesnrUtil;
	
	private static final Logger logger = LogManager.getLogger(InstituteController.class);
	
	@RequestMapping(value = {"/instituteRegistration" }, method = RequestMethod.POST)
	public String instituteRegistration(ModelMap model, @RequestParam(value = "error",required = false) String error,
			@ModelAttribute("user") User user,HttpServletRequest request, HttpServletResponse response,@CookieValue("JSESSIONID") String cookie) {
		
		boolean acceptedTerms = true;
		boolean validateUser = validateUser(user);
		
		if(user != null && !user.isAgreeTerms()){
			logger.info("Terms and Conditions not accepted");
			acceptedTerms = false;
			model.addAttribute("msg", QuesnrConstants.PLEASE_ACCEPT_TERMS);
			model.addAttribute("role",QuesnrConstants.REGISTER_AS_INSTITUTE);
			return "welcomePages/registration-block";
		}
		
		if(validateUser){
			logger.info("User is Validated");
			User findUser = instituteService.findUserByUserName(user.getUsername());
			if(findUser!=null){
				if(findUser.getUsername().equals(user.getUsername())){
					logger.info("User not Validated");
					model.addAttribute("msg", QuesnrConstants.USERNAME_ALREADY_EXIST );
					model.addAttribute("role",QuesnrConstants.REGISTER_AS_INSTITUTE);
					return "welcomePages/registration-block";
				}else{
					try {
						user = setInstituteDetails(user, cookie);
					} catch (QuesnrException e1) {
						// TODO Auto-generated catch block
						logger.info("",e1.getErrorCode()+ " " +e1.getMessage());
						model.addAttribute("errorCode",e1.getErrorCode());
						model.addAttribute("An error occured while processing your request.", e1.getMessage());
						return "welcomePages/error-page";
					}
					instituteService.createInstitute(user);
					try {
						emailService.sendEmail(user, user.getInstitute().getEmail());
					} catch (QuesnrException e) {
						// TODO Auto-generated catch block
						logger.info("",e.getErrorCode()+ " " +e.getMessage());
						model.addAttribute("errorCode",e.getErrorCode());
						model.addAttribute("An error occured while processing your request.", e.getMessage());
						return "welcomePages/error-page";
					}
					
					model.addAttribute("msg", QuesnrConstants.PLEASE_VERIFY_ACCOUNT);
					return "welcomePages/entry";
				}
			}else{
				try {
					user = setInstituteDetails(user, cookie);
				} catch (QuesnrException e1) {
					// TODO Auto-generated catch block
					logger.info("",e1.getErrorCode()+ " " +e1.getMessage());
					model.addAttribute("errorCode",e1.getErrorCode());
					model.addAttribute("An error occured while processing your request.", e1.getMessage());
					return "welcomePages/error-page";
				}
				instituteService.createInstitute(user);
				try {
					emailService.sendEmail(user, user.getInstitute().getEmail());
				} catch (QuesnrException e) {
					// TODO Auto-generated catch block
					logger.info("",e.getErrorCode()+ " " +e.getMessage());
					model.addAttribute("errorCode",e.getErrorCode());
					model.addAttribute("An error occured while processing your request.", e.getMessage());
					return "welcomePages/error-page";
				}
				
				model.addAttribute("msg", QuesnrConstants.PLEASE_VERIFY_ACCOUNT);
				return "welcomePages/entry";
			}
		}else{
			logger.info("User not Validated");
			model.addAttribute("msg", QuesnrConstants.PLEASE_FILL_ALL_FIELDS );
			model.addAttribute("role",QuesnrConstants.REGISTER_AS_INSTITUTE);
			return "welcomePages/registration-block";
		}
		
	}
	
	private User setInstituteDetails(User user,String cookie ) throws QuesnrException{
		// TODO Auto-generated method stub
		String ipAddress = quesnrUtil.getIpAddress();
		String hashPassword = hashCode.getHashPassword(user.getPassword());
		user.setPassword(hashPassword);
		user.setEnabled(false);
		user.setVerified(false);
		user.setIpAddress(ipAddress);
		user.setjSessionId(cookie);
		user.setUpdatedBy(QuesnrConstants.ROLE_INSTITUTE);
		user.setCreatedBy(QuesnrConstants.ROLE_INSTITUTE);
		user.setRole(QuesnrConstants.INSTITUTE);
		Date dateCreated = new Date();  
		user.setDateCreated(dateCreated);
		Date updatedDate = new Date();  
		user.setUpdatedDate(updatedDate);
		user.setLastLoggedInTime(updatedDate);
		UserRole userRole = new UserRole();
		userRole.setUserRole(QuesnrConstants.ROLE_INSTITUTE);
		userRole.setRole(QuesnrConstants.ROLE_INSTITUTE);
		Set<UserRole> roles = new HashSet<UserRole>();
		roles.add(userRole);
		user.setUserRole(roles);
		return user;
	}
	
	public String getIpAddress() throws QuesnrException {
		// TODO Auto-generated method stub
		InetAddress ip = null;
		 try {
			ip = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			throw new QuesnrException("",e.getMessage());
		}
		return ip.getHostAddress();
	}
	
	private boolean validateUser(User user) {
		logger.info("Validating the User");
		boolean isValidated = true;
		if(user.getUsername().equals(null) || user.getUsername().equals("")){
			isValidated = false;
		}
		if(user.getPassword().equals(null) || user.getPassword().equals("")){
			isValidated = false;
		}
		if(user.getInstitute().getInstituteName().equals(null) || user.getInstitute().getInstituteName().equals("")){
			isValidated = false;
		}
		if(user.getInstitute().getEmail().equals(null) || user.getInstitute().getEmail().equals("")){
			isValidated = false;
		}
		if(user.getInstitute().getContactNo().equals(null) || user.getInstitute().getContactNo().equals("")){
			isValidated = false;
		}
	
		return isValidated;
	}
	
	@ExceptionHandler({IOException.class, Exception.class})
    public ModelAndView handleIOException(Exception ex) {
        ModelAndView model = new ModelAndView("welcomePages/error-page");
 
        model.addObject("An error occured while processing your request.", ex.getMessage());
         
        return model;
    }
}
